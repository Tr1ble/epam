package by.training.project.builder;

import by.training.project.entities.Student;
import by.training.project.entities.enums.StudentEnum;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class StudentBuilderTest extends Assert {

    @Mock
    private ResultSet resultSet;

    private static final Student CORRECT_STUDENT = new Student("Artem", "Sadovsky",
         "Evgenevich",10, "Tr1ble", 10);

    private static final Student INCORRECT_STUDENT = new Student("Artem", "Sadovsky",
            "Evgenevich",10, "Tr1ble", 11);

    private static final Builder builder = new StudentBuilder();

    private Student builtStudent;

    @Before
    public void setUp() throws SQLException {
        resultSet = mock(ResultSet.class);

        when(resultSet.getInt(StudentEnum.ID.getValue())).thenReturn(10);
        when(resultSet.getString(StudentEnum.SURNAME.getValue())).thenReturn("Sadovsky");
        when(resultSet.getString(StudentEnum.FIRSTNAME.getValue())).thenReturn("Artem");
        when(resultSet.getString(StudentEnum.SECONDNAME.getValue())).thenReturn("Evgenevich");
        when(resultSet.getInt(StudentEnum.COURSE_ID.getValue())).thenReturn(10);
        when(resultSet.getString(StudentEnum.USER_LOGIN.getValue())).thenReturn("Tr1ble");

        builtStudent = (Student) builder.build(resultSet);
    }

    @Test
    public void testBuilderObjectShouldBeEqual() throws SQLException {
        assertEquals(builtStudent, CORRECT_STUDENT);
    }
    @Test
    public void testBuilderObjectShouldBeNotEqual() throws SQLException {
        assertNotEquals(builder.build(resultSet), INCORRECT_STUDENT);
    }
}