package by.training.project.io.validator;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(DataProviderRunner.class)
public class DateValidatorTest extends Assert {

    @DataProvider
    public static Object[][] getValidData() {
        return new Object[][] {
                {"2005-10-10"},
                {"2015-01-02"},
                {"2000-12-12"},
                {"2019-09-09"}
        };
    }
    @DataProvider
    public static Object[][] getInvalidData() {
        return new Object[][] {
                {"20-10-10"},
                {"20151-01-02"},
                {"2000-13-13"},
                {"2019-09-0"}
        };
    }


    @Mock
    private Validator validator;

    @Before
    public void setUp(){
        validator = new DateValidator();
    }

    @Test
    @UseDataProvider("getValidData")
    public void testStringShouldBeValid(String date) {
        assertTrue(validator.isValid(date));
    }
    @Test
    @UseDataProvider("getInvalidData")
    public void testStringShouldBeInvalid(String date) {
        assertFalse(validator.isValid(date));
    }

}