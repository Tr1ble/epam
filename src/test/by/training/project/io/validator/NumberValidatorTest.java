package by.training.project.io.validator;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.junit.Assert.*;


@RunWith(DataProviderRunner.class)
public class NumberValidatorTest extends Assert {

    @DataProvider
    public static Object[][] getValidData() {
        return new Object[][] {
                {"2005"},
                {"2021321312"},
                {"0"},
                {"20142"},
                {"9131"}
        };
    }
    @DataProvider
    public static Object[][] getInvalidData() {
        return new Object[][] {
                {"20-10-10"},
                {"wa02"},
                {"$"},
                {"O"},
                {"+1"}
        };
    }


    @Mock
    private Validator validator;

    @Before
    public void setUp(){
        validator = new NumberValidator();
    }

    @Test
    @UseDataProvider("getValidData")
    public void testStringShouldBeValid(String date) {
        assertTrue(validator.isValid(date));
    }
    @Test
    @UseDataProvider("getInvalidData")
    public void testStringShouldBeInvalid(String date) {
        assertFalse(validator.isValid(date));
    }

}