package by.training.project.web;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.database.ConnectionPool;
import by.training.project.database.ProxyConnection;
import by.training.project.repository.factory.RepositoryFactory;
import org.apache.log4j.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * This is servlet based on Front Controller pattern. So
 * he processing all requests.
 */
public class FrontController extends HttpServlet {

    private final static Logger LOG = Logger.getLogger(FrontController.class.getName());
    private static final String CURRENT_PAGE = "currentPage";
    private static final String ERROR_PAGE = "/WEB-INF/error.jsp";
    private static final String COMMAND_PARAMETER = "command";
    private static final String ERROR_MESSAGE_ATTRIBUTE = "errorMessage";


    /**
     * This method is responsible for catching of post request
     * @param req request variable
     * @param res response variable
     * @throws IOException if
     * @throws ServletException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        process(req, res);

    }

    /**
     * This method is responsible for catching of get request
     * @param req request variable
     * @param res response variable
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        process(req, res);
    }

    /**
     * This private method is processing all both
     * post and get requests. First of all he
     * gets connection from connection pool. Then he
     * gets command parameter and creates match command
     * object. After it he executes it and dispatch user
     * to some page.
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        Command command;
        CommandResult commandResult = new CommandResult(ERROR_PAGE, false);;
        try (ProxyConnection connection = connectionPool.getConnection()) {

            RepositoryFactory factory = new RepositoryFactory(connection);
            String action = request.getParameter(COMMAND_PARAMETER);

            CommandFactory commandFactory = new CommandFactory(factory);
            command = commandFactory.getCommand(action);
            commandResult = command.execute(request, response);
        } catch (Exception exception) {
            LOG.error(exception.getMessage());
            commandResult = new CommandResult(ERROR_PAGE, false);
            request.setAttribute(ERROR_MESSAGE_ATTRIBUTE, exception.getMessage());
        } finally {
            dispatch(request, response, commandResult);
        }
    }

    /**
     * This is method that is responsible for dispatching
     * of user to appropriate page by one of the way: redirect or
     * forward
     * @param request request variable
     * @param response response variable
     * @param commandResult command result that has target page and way of response
     * @throws IOException may be when sending redirect or forwarding
     * @throws ServletException may be when forwarding
     */
    private void dispatch(HttpServletRequest request, HttpServletResponse response, CommandResult commandResult) throws IOException, ServletException {
        String page = commandResult.getPage();
        request.getSession().setAttribute(CURRENT_PAGE, page);
        if (commandResult.isRedirect()) {
            response.sendRedirect(request.getContextPath() + page);
        } else {
            request.getRequestDispatcher(page).forward(request, response);
        }
    }
}
