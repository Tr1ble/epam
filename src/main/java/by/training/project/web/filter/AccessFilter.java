package by.training.project.web.filter;


import by.training.project.command.CommandFactory;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.student.StudentCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.User;
import by.training.project.repository.factory.RepositoryFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "AccessFilter", urlPatterns = {"/*"})
public class AccessFilter implements Filter {

    private static final String HOME_PAGE = "/index.jsp";
    private static final String USER_ATTRIBUTE = "user";
    private static final String COMMAND_PARAMETER = "command";
    private static final String LOGIN_COMMAND = "login";

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        Object attribute = req.getSession().getAttribute(USER_ATTRIBUTE);

        String command = req.getParameter(COMMAND_PARAMETER);
        CommandFactory commandFactory=new CommandFactory(new RepositoryFactory());

        int flag = 0;


        if (attribute !=null && command!=null && !command.equals(LOGIN_COMMAND)) {
            User user = (User) attribute;
            String contextPath = req.getContextPath();
            switch (user.getRole()) {
                case ADMINISTRATOR:
                    if(!(commandFactory.getCommand(command) instanceof AdminCommand)) {
                        flag=1;
                        res.sendRedirect(contextPath +HOME_PAGE);
                    }
                    break;
                case TRAINER:
                    if(!(commandFactory.getCommand(command) instanceof TrainerCommand)) {
                        flag=1;
                        res.sendRedirect(contextPath +HOME_PAGE);
                    }
                    break;
                case STUDENT:
                    if(!(commandFactory.getCommand(command) instanceof StudentCommand)) {
                        flag=1;
                        res.sendRedirect(contextPath +HOME_PAGE);
                    }
                    break;
                    default:
                        flag=1;
                        res.sendRedirect(contextPath + HOME_PAGE);
                        break;
            }
        }
        if(flag==0) {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {

    }
}
