package by.training.project.web.filter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "LocaleFilter", urlPatterns = {"/*"})
public class LocaleFilter implements Filter {

    private static final String INDEX_JSP = "index.jsp";
    private static final String LANG = "lang";
    private static final String EN = "en";
    private static final String SESSION_LOCALE = "sessionLocale";
    private static final String CURRENT_PAGE = "currentPage";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        if (session.getAttribute(LANG) == null) {
            session.setAttribute(LANG, EN);
        }
        if (req.getParameter(SESSION_LOCALE) != null) {
            session.setAttribute(LANG, req.getParameter(SESSION_LOCALE));
            Object currentPageObj = session.getAttribute(CURRENT_PAGE);
            String currentPage;
            if (currentPageObj != null) {
                currentPage = currentPageObj.toString();
            } else {
                currentPage = INDEX_JSP;
            }
            request.getRequestDispatcher(currentPage).forward(request, response);
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
