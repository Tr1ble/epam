package by.training.project.web.tag;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

public class LanguageTag extends TagSupport {
    @Override
    public int doStartTag() {
        JspWriter writer = pageContext.getOut();
            try {
            writer.write("<div class=\"lang-container\">");
            writer.write("<a href=\"${pageContext.request.contextPath}?sessionLocale=ru\">" +
                    "<img alt=\"russian_lang\"" +
                    " class=\"lang\"\n" +
                    "src=\"\\training\\stylesheets\\images\\443648291_flag-rossii-.jpg\"" +
                    " width=\"50\"\n" +
                    "height=\"35\">" +
                    "</a>");
            writer.write("<a href=\"${pageContext.request.contextPath}?sessionLocale=en\"> <img alt=\"english_lang\"" +
                    " class=\"lang\"\n" +
                    "src=\"\\training\\stylesheets\\images\\Flag_of_the_United_States_(Pantone).svg\"\n" +
                    "width=\"50\" height=\"35\">\n" +
                    "</a></div>");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }
}
