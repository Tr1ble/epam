package by.training.project.io.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface Reader {
    Map<String, String> read(InputStream stream) throws IOException;
}
