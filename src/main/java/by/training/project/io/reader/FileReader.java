package by.training.project.io.reader;


import by.training.project.exceptions.FileIsEmptyException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileReader implements Reader {


    private static final String SPLIT_REGEX = "==";

    @Override
    public Map<String, String> read(InputStream stream) throws FileIsEmptyException, IOException {
        BufferedReader br = null;
        Map<String, String> data = new HashMap<>(5);
        try {
            br = new BufferedReader(new InputStreamReader(stream));
            String temp;
            while ((temp = br.readLine()) != null) {
                String[] parts = temp.split(SPLIT_REGEX);
                data.put(parts[0], parts[1]);
            }
        } finally {
            if (br != null) {
                br.close();
            }
            if (data.size() == 0) {
                throw new FileIsEmptyException();
            }
            return data;
        }
    }
}
