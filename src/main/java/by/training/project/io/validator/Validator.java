package by.training.project.io.validator;

public interface Validator {
    boolean isValid(String data);
}
