package by.training.project.io.validator;

public class DateValidator implements Validator{
    private static final String DATE_PATTERN= "([12]\\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]))";

    @Override
    public boolean isValid(String data) {
        return data.matches(DATE_PATTERN);
    }
}
