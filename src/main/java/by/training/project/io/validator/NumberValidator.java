package by.training.project.io.validator;

public class NumberValidator implements Validator{

    private static final String NUMBER_PATTERN = "\\d*";

    @Override
    public boolean isValid(String data) {
        return data.matches(NUMBER_PATTERN);
    }
}
