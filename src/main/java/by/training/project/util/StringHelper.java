package by.training.project.util;

public class StringHelper {
    public String[] splitStringInto3Words(String data) {
        String[] words = data.split(" ");
        String first = words[0];
        String second = words[1];
        String third = words[2];
        return new String[] {
                first, second, third
        };
    }
}
