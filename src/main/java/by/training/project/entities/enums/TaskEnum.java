package by.training.project.entities.enums;

public enum TaskEnum {
    ID("id"),
    TITLE("title"),
    COURSE_ID("course_id"),
    DESCRIPTION("task_description");

    private String value;

    TaskEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
