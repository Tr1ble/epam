package by.training.project.entities.enums;

import java.io.Serializable;

public enum CompletedTaskEnum {
    ID("id"),
    COURSE_ID("course_id"),
    TASK_ID("task_id"),
    DESCRIPTION("task_description"),
    MARK("mark"),
    FEEDBACK("feedback"),
    STUDENT("student");

    private String value;

    CompletedTaskEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
