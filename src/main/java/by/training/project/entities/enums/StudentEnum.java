package by.training.project.entities.enums;

public enum StudentEnum {
    ID("id"),
    SURNAME("surname"),
    FIRSTNAME("firstname"),
    SECONDNAME("secondname"),
    COURSE_ID("course_id"),
    USER_LOGIN("user_login");

    private String value;

    StudentEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
