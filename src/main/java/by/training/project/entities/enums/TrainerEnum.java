package by.training.project.entities.enums;

public enum TrainerEnum {
    ID("id"),
    SURNAME("surname"),
    FIRSTNAME("firstname"),
    SECONDNAME("secondname"),
    ISBUSY("isBusy"),
    USER_LOGIN("user_login");

    private String value;

    TrainerEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
