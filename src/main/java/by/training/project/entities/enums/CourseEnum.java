package by.training.project.entities.enums;

public enum CourseEnum {
    ID("id"),
    TRAINER_ID("trainer_id"),
    TITLE("title"),
    START_DATE("start_date"),
    END_DATE("end_date");

    private String value;

    CourseEnum (String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
