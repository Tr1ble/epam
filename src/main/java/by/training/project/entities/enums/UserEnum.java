package by.training.project.entities.enums;

public enum UserEnum {
    ID("id"),
    LOGIN("login"),
    PASSWORD("password"),
    ROLE("role");

    private String value;

    UserEnum(String value) {
        this.value=value;
    }

    public String getValue() {
        return value;
    }
}
