package by.training.project.entities;

public class Man {

    private String firstname;
    private String surname;
    private String secondname;

    public Man() {
    }

    public Man(String firstname, String surname, String secondname) {
        this.firstname = firstname;
        this.surname = surname;
        this.secondname = secondname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }
}
