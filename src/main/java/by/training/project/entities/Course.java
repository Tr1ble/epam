package by.training.project.entities;

import java.util.Date;
import java.util.Objects;

public class Course implements Identifable{
    private int id;
    private int trainerId;
    private String title;
    private Date startDate;
    private Date endDate;

    @Override
    public Integer  getId() {
        return id;
    }

    public Course(int id, int trainerId, String title, Date startDate, Date endDate) {
        this.id=id;
        this.trainerId = trainerId;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Course(int trainerId, String title, Date startDate, Date endDate) {
        this.id=id;
        this.trainerId = trainerId;
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getTrainerId() {
        return trainerId;
    }

    public void setTrainerId(int trainerId) {
        this.trainerId = trainerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }


    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", trainerId=" + trainerId +
                ", title='" + title + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Course course = (Course) o;
        return id == course.id &&
                trainerId == course.trainerId &&
                Objects.equals(title, course.title) &&
                Objects.equals(startDate, course.startDate) &&
                Objects.equals(endDate, course.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, trainerId, title, startDate, endDate);
    }
}
