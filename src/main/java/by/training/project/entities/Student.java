package by.training.project.entities;

import java.util.Objects;

public class Student extends Man implements Identifable {

    private int id;
    private String user;
    private int courseId;


    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public Student() {
    }

    public Student(int id, String user) {
        this.id = id;
        this.user = user;
    }

    public Student(int id, String user, int courseId) {
        this.id = id;
        this.user = user;
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", user='" + user + '\'' +
                ", courseId=" + courseId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id &&
                courseId == student.courseId &&
                Objects.equals(user, student.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, courseId);
    }

    public Student(String firstname, String surname, String secondname, String user, int courseId) {
        super(firstname, surname, secondname);
        this.user = user;
        this.courseId = courseId;
    }

    public Student(String firstname, String surname, String secondname, int id, String user, int courseId) {
        super(firstname, surname, secondname);
        this.id = id;
        this.user = user;
        this.courseId = courseId;
    }

    public Student(String user) {
        this.id = id;
        this.user = user;
    }

    public Student(String firstname, String surname, String secondname, int id, String user) {
        super(firstname, surname, secondname);
        this.id = id;
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student(int id) {
        this.id = id;
    }

    public Student(String firstname, String surname, String secondname, int id) {
        super(firstname, surname, secondname);
        this.id = id;
    }
}
