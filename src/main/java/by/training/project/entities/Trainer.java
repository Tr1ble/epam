package by.training.project.entities;

import java.util.Objects;

public class Trainer extends Man implements Identifable{

    private  int id;
    private boolean isBusy;
    private String userLogin;



    public Trainer(String firstname, String surname, String secondname, int id, boolean isBusy, String userLogin) {
        super(firstname, surname, secondname);
        this.id = id;
        this.isBusy = isBusy;
        this.userLogin = userLogin;
    }

    public Trainer(String firstname, String surname, String secondname, boolean isBusy, String userLogin) {
        super(firstname, surname, secondname);
        this.isBusy = isBusy;
        this.userLogin = userLogin;
    }

    public Trainer(String firstname, String surname, String secondname, String userLogin) {
        super(firstname, surname, secondname);
        this.userLogin = userLogin;
    }


    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void setBusy(boolean busy) {
        isBusy = busy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trainer trainer = (Trainer) o;
        return id == trainer.id &&
                isBusy == trainer.isBusy &&
                userLogin == trainer.userLogin;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isBusy, userLogin);
    }

    @Override
    public String toString() {
        return "Trainer{" +
                "id=" + id +
                ", isBusy=" + isBusy +
                ", userLogin=" + userLogin +
                '}';
    }
}
