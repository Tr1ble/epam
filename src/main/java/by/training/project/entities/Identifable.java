package by.training.project.entities;

public interface Identifable {
    Integer getId();
}
