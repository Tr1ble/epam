package by.training.project.entities;

import java.util.Objects;

public class Task implements Identifable{
    private int id;
    private String title;
    private int courseId;
    private String description;

    @Override
    public Integer getId() {
        return id;
    }

    public Task() {
    }

    public Task(int id) {
        this.id = id;
    }

    public Task(int courseId, String description) {
        this.courseId = courseId;
        this.description = description;
    }
    public Task(int id, int courseId, String description) {
        this.id = id;
        this.courseId = courseId;
        this.description = description;
    }

    public Task(String title, int courseId, String description) {
        this.title = title;
        this.courseId = courseId;
        this.description = description;
    }

    public Task(int id, String title, int courseId, String description) {
        this.id = id;
        this.title = title;
        this.courseId = courseId;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourseId() {
        return courseId;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                courseId == task.courseId &&
                Objects.equals(description, task.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, courseId, description);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", courseId=" + courseId +
                ", description='" + description + '\'' +
                '}';
    }
}
