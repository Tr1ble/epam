package by.training.project.entities;

public enum Role {
    ADMINISTRATOR("administrator"),
    STUDENT("student"),
    TRAINER("trainer");
    String value;

    Role(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
