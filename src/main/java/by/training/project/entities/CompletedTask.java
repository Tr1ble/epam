package by.training.project.entities;

public class CompletedTask extends Task {

   private double mark;
   private String feedback;
   private String student;

   public CompletedTask(int id, double mark, String feedback, String student) {
      super(id);
      this.mark = mark;
      this.feedback = feedback;
      this.student = student;
   }

   public CompletedTask(int courseId, String description, double mark, String feedback, String student) {
      super(courseId, description);
      this.mark = mark;
      this.feedback = feedback;
      this.student = student;
   }

   public CompletedTask(int id, int courseId, String description, double mark, String feedback, String student) {
      super(id, courseId, description);
      this.mark = mark;
      this.feedback = feedback;
      this.student = student;
   }

   public CompletedTask(int id, String title, int courseId, String description, double mark, String feedback, String student) {
      super(id, title, courseId, description);
      this.mark = mark;
      this.feedback = feedback;
      this.student = student;
   }

   public CompletedTask(double mark, String feedback, String student) {
      this.mark = mark;
      this.feedback = feedback;
      this.student = student;
   }

   public Integer getId() {
     return super.getId();
   }

   public double getMark() {
      return mark;
   }

   public void setMark(double mark) {
      this.mark = mark;
   }

   public String getFeedback() {
      return feedback;
   }

   public void setFeedback(String feedback) {
      this.feedback = feedback;
   }

   public String getStudent() {
      return student;
   }

   public void setStudent(String student) {
      this.student = student;
   }
}
