package by.training.project.database;

import by.training.project.io.reader.FileReader;
import by.training.project.io.reader.Reader;

import javax.naming.NamingException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

public class ConnectionPool {

    private static final int CONNECTIONS_NUMBER = 5;
    private static final String DATABASE_PROPERTIES = "/database.properties";
    private static final String PASSWORD = "password";
    private static final String USER = "user";
    private static final String URL = "url";
    private static AtomicBoolean init=new AtomicBoolean(false);
    private static ConnectionPool instance;
    private static final ReentrantLock INSTANCE_LOCK =new ReentrantLock();
    private static final ReentrantLock LOCK =new ReentrantLock();

    private Queue<ProxyConnection> connections;
    private List<ProxyConnection> usedConnections;

    private Reader reader = new FileReader();

    private ConnectionPool() { }

    public static ConnectionPool getInstance() {
        if(!init.get()) {
            INSTANCE_LOCK.lock();
            try {
                if (!init.get()) {
                    final ConnectionPool temp = new ConnectionPool();
                    instance = temp;
                    instance.init();
                    init.set(true);
                }
            }  finally {
                INSTANCE_LOCK.unlock();
            }
        }
        return instance;
    }

    private void init()  {
        connections=new ArrayDeque<>(5);
        usedConnections=new ArrayList<>(5);
        try {
            Map<String, String> props=reader.read(getClass().getClassLoader().getResourceAsStream(DATABASE_PROPERTIES));
            for (int i = 0; i < CONNECTIONS_NUMBER; i++) {
                Connection connection = DriverManager.getConnection(props.get(URL), props.get(USER), props.get(PASSWORD));
                connections.add(new ProxyConnection(connection));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public ProxyConnection getConnection() {
        LOCK.lock();
        ProxyConnection connection;
        try {
            connection = connections.poll();
            usedConnections.add(connection);
        } finally {
            LOCK.unlock();
        }

        return connection;
    }
    public void returnConnection(ProxyConnection connection) {

        LOCK.lock();

        try {
            if (usedConnections.contains(connection)) {
                connections.offer(connection);
                usedConnections.remove(connection);
            } else {
                throw  new IllegalArgumentException("");
            }
        } finally {
            LOCK.unlock();
        }
    }
}
