package by.training.project.service;

import by.training.project.entities.CompletedTask;
import by.training.project.entities.Course;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.completedtask.CompletedTaskRepositoryImpl;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.completedtask.CompletedTaskSpecificationByCourse;
import by.training.project.repository.specification.completedtask.CompletedTaskSpecificationByID;
import by.training.project.repository.specification.course.CourseSpecificationByID;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

public class CompletedTaskService {
    private RepositoryFactory repositoryFactory;
    private CompletedTaskRepositoryImpl repository;

    public CompletedTaskService(RepositoryFactory repositoryFactory) throws IllegalArgumentException, IllegalArgumentException {
        this.repositoryFactory = repositoryFactory;
        repository = (CompletedTaskRepositoryImpl) repositoryFactory.create(RepositoryType.COMPLETED_TASK);
    }

    public List<CompletedTask> getAllCompletedTasks() throws  RepositoryException {
        return repository.getAll();
    }

    public void addCompletedTask(CompletedTask completedTask) throws RepositoryException {
        repository.add(completedTask);
    }

    public void removeCompletedTask(int id) throws RepositoryException {
        repository.remove(new CompletedTaskSpecificationByID(id));
    }

    public List<CompletedTask> getCompletedTasksByCourse(int courseId) throws  RepositoryException {
        return repository.findCompletedTasks(new CompletedTaskSpecificationByCourse(courseId));
    }
}
