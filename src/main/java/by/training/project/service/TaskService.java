package by.training.project.service;

import by.training.project.entities.Task;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.course.CourseRepositoryImpl;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.task.TaskSpecification;
import by.training.project.repository.specification.task.TaskSpecificationByID;
import by.training.project.repository.task.TaskRepositoryImpl;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public class TaskService {

    private RepositoryFactory repositoryFactory;
    private TaskRepositoryImpl repository;


    public TaskService(RepositoryFactory repositoryFactory) throws IllegalArgumentException {
        this.repositoryFactory = repositoryFactory;
        this.repository = (TaskRepositoryImpl) repositoryFactory.create(RepositoryType.TASK);
    }

    public List<Task> getAllTasks() throws RepositoryException {
        return repository.getAll();
    }

    public void removeTask(int id) throws RepositoryException {
        repository.remove(new TaskSpecificationByID(id));
    }

    public void addTask(Task task) throws RepositoryException {
        repository.add(task);
    }

    public Optional<Task> findTaskById(int taskId) throws RepositoryException {
        return repository.findTaskById(new TaskSpecificationByID(taskId));
    }
}
