package by.training.project.service;

import by.training.project.entities.Course;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.course.CourseRepositoryImpl;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.course.CourseSpecificationByID;
import java.util.List;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public class CourseService {

    private RepositoryFactory repositoryFactory;
    private CourseRepositoryImpl repository;

    public CourseService(RepositoryFactory repositoryFactory) throws IllegalArgumentException {
        this.repositoryFactory = repositoryFactory;
        repository = (CourseRepositoryImpl) repositoryFactory.create(RepositoryType.COURSE);
    }

    public List<Course> getAllCourses() throws RepositoryException {
        return repository.getAll();
    }

    public void addCourse(Course course) throws RepositoryException {
        repository.add(course);
    }

    public void removeCourse(int id) throws RepositoryException {
        repository.remove(new CourseSpecificationByID(id));
    }

    public Optional<Course> findCourseById(int id) throws RepositoryException {
        return repository.findCourse(new CourseSpecificationByID(id));
    }

    public void updateCourse(Course course, int id) throws RepositoryException {
        repository.update(course, new CourseSpecificationByID(id));
    }
}
