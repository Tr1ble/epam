package by.training.project.service;


import by.training.project.entities.User;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.user.UserRepositoryImpl;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.user.UserSpecificationByLoginAndPassword;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public class UserService {

    private RepositoryFactory repositoryFactory;

    public UserService(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Optional<User> login(String login, String password) throws RepositoryException, IllegalArgumentException {
        UserRepositoryImpl repository= (UserRepositoryImpl) repositoryFactory.create(RepositoryType.USER);
        return repository.findUser(new UserSpecificationByLoginAndPassword(login, password));
    }

}
