package by.training.project.service;

import by.training.project.entities.Student;
import by.training.project.entities.Task;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.student.StudentSpecificationByCourseID;
import by.training.project.repository.specification.student.StudentSpecificationByID;
import by.training.project.repository.specification.student.StudentSpecificationByLogin;
import by.training.project.repository.specification.student.StudentSpecificationByName;
import by.training.project.repository.specification.task.TaskSpecificationByID;
import by.training.project.repository.student.StudentRepositoryImpl;
import by.training.project.repository.task.TaskRepositoryImpl;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public class StudentService {

    private RepositoryFactory repositoryFactory;
    private StudentRepositoryImpl repository;


    public StudentService(RepositoryFactory repositoryFactory) throws IllegalArgumentException {
        this.repositoryFactory = repositoryFactory;
        this.repository = (StudentRepositoryImpl) repositoryFactory.create(RepositoryType.STUDENT);
    }


    public List<Student> getAllStudents() throws RepositoryException {
        return repository.getAll();
    }

    public void removeTask(int id) throws RepositoryException {
        repository.remove(new StudentSpecificationByID(id));
    }

    public void register(Student student) throws RepositoryException {
        repository.add(student);
    }
    public Optional<Student> findCurrentStudentByLogin(String login) throws IllegalArgumentException, RepositoryException {
        repository= (StudentRepositoryImpl) repositoryFactory.create(RepositoryType.STUDENT);
        return repository.findStudent(new StudentSpecificationByLogin(login));
    }

    public void removeStudentByCourseId(int courseId) throws RepositoryException {
        repository.remove(new StudentSpecificationByCourseID(courseId));
    }

    public Optional<Student> getTrainerByFio(String surname, String firstname, String secname) throws RepositoryException {
        return repository.findStudent(new StudentSpecificationByName(surname, firstname, secname));
    }
}
