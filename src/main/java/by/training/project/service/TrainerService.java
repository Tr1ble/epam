package by.training.project.service;

import by.training.project.entities.Trainer;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.repository.factory.RepositoryType;
import by.training.project.repository.specification.trainer.TrainerSpecificationByBusy;
import by.training.project.repository.specification.trainer.TrainerSpecificationByID;
import by.training.project.repository.specification.trainer.TrainerSpecificationByName;
import by.training.project.repository.specification.trainer.TrainerSpecificationByUserLogin;
import by.training.project.repository.trainer.TrainerRepositoryImpl;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.*;

public class TrainerService {

    private RepositoryFactory repositoryFactory;
    private TrainerRepositoryImpl repository;

    public TrainerService(RepositoryFactory repositoryFactory) throws IllegalArgumentException {
        this.repositoryFactory = repositoryFactory;
        repository = (TrainerRepositoryImpl) repositoryFactory.create(RepositoryType.TRAINER);
    }

    public List<Trainer> getAllTrainers() throws RepositoryException {
        return repository.getAll();
    }

    public void addTrainer(Trainer trainer) throws RepositoryException {
        trainer.setBusy(false);
        repository.add(trainer);
    }


    public void removeTrainer(int id) throws RepositoryException {
        repository.remove(new TrainerSpecificationByID(id));
    }

    public Optional<Trainer> getTrainerByID(int id) throws RepositoryException {
        return repository.findTrainer(new TrainerSpecificationByID(id));
    }

    public void changeTrainerBusy(int id, boolean isBusy) throws RepositoryException {
        Optional<Trainer> trainerByID = getTrainerByID(id);
        Trainer trainer;
        if(trainerByID.isPresent()) {
            trainer= trainerByID.get();
            trainer.setBusy(isBusy);
            repository.update(trainer, new TrainerSpecificationByID(id));
        }
    }

    public Optional<Trainer> findCurrentTrainerByUserLogin(String login) throws IllegalArgumentException,  RepositoryException {
        repository= (TrainerRepositoryImpl) repositoryFactory.create(RepositoryType.TRAINER);
        return repository.findTrainer(new TrainerSpecificationByUserLogin(login));
    }

    public Optional<Trainer> getTrainerByFio(String surname, String firstname, String secname) throws RepositoryException {
        return repository.findTrainer(new TrainerSpecificationByName(surname, firstname, secname));
    }

    public List<Trainer> getTrainerByBusy(boolean busy) throws RepositoryException {
        return repository.findTrainers(new TrainerSpecificationByBusy(busy));
    }

    public void updateTrainer(Trainer trainer, int id) throws RepositoryException {
        repository.update(trainer, new TrainerSpecificationByID(id));
    }
}
