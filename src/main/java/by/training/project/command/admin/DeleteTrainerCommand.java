package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TrainerService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Delete trainer command.
 */
public class DeleteTrainerCommand implements AdminCommand {

    /**
     * The constant TRAINER_ID.
     */
    private static final String TRAINER_ID = "trainerId";
    /**
     * The constant TRAINERS_PAGE_JSP.
     */
    private static final String TRAINERS_PAGE_JSP = "/trainers-page.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Delete trainer command.
     *
     * @param repositoryFactory the repository factory
     */
    public DeleteTrainerCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        TrainerService service=new TrainerService(repositoryFactory);
        String trainerId = request.getParameter(TRAINER_ID);
        service.removeTrainer(Integer.parseInt(trainerId));

        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        return new CommandResult(TRAINERS_PAGE_JSP, true);
    }
}
