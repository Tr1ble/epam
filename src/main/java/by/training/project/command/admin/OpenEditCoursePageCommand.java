package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.entities.Course;
import by.training.project.entities.Trainer;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CourseService;
import by.training.project.service.TrainerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Optional;

/**
 * The type Open add course page command.
 */
public class OpenEditCoursePageCommand implements AdminCommand {

    /**
     * The constant ADD_COURSE_PAGE.
     */
    private static final String ADD_COURSE_PAGE = "/WEB-INF/edit-course.jsp";
    private static final String COURSE_ID = "courseId";
    private static final String TITLE= "title";
    private static final String TRAINER_ID = "trainerId";
    private static final String TRAINER_SURNAME = "surname";
    private static final String TRAINER_NAME = "name";
    private static final String TRAINER_SECNAME = "secname";
    private static final String START_DATE= "startDate";
    private static final String END_DATE = "endDate";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Open add course page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenEditCoursePageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        String courseId = request.getParameter(COURSE_ID);
        request.setAttribute(COURSE_ID, courseId);

        CourseService courseService = new CourseService(repositoryFactory);

        Optional<Course> course = courseService.findCourseById(Integer.parseInt(courseId));
        if(course.isPresent()) {
            int trainerId = course.get().getTrainerId();
            request.setAttribute(TRAINER_ID, trainerId);
            request.setAttribute(TITLE, course.get().getTitle());
            request.setAttribute(START_DATE, course.get().getStartDate());
            request.setAttribute(END_DATE, course.get().getEndDate());

            TrainerService trainerService=new TrainerService(repositoryFactory);
            Optional<Trainer> trainer=trainerService.getTrainerByID(trainerId);
            trainer.ifPresent(trainer1 -> setTrainerFio(trainer1, request));

        }

        return new CommandResult(ADD_COURSE_PAGE, false);
    }

    private void setTrainerFio(Trainer trainer, HttpServletRequest request) {
        request.setAttribute(TRAINER_SURNAME, trainer.getSurname());
        request.setAttribute(TRAINER_NAME, trainer.getFirstname());
        request.setAttribute(TRAINER_SECNAME, trainer.getSecondname());
    }
}
