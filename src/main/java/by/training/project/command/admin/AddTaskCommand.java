package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetTasksCommand;
import by.training.project.entities.Task;
import by.training.project.exceptions.InvalidDataException;
import by.training.project.io.validator.NumberValidator;
import by.training.project.io.validator.Validator;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Add task command.
 */
public class AddTaskCommand implements AdminCommand {

    private RepositoryFactory repositoryFactory;
    private Validator numberValidator = new NumberValidator();

    private static final String PARAMETER_TITLE = "title";
    private static final String PARAMETER_DESCRIPTION = "description";
    private static final String PARAMETER_COURSE_ID  = "courseId";
    private static final String TARGET_PAGE = "/training-page.jsp";

    /**
     * Instantiates a new Add task command.
     *
     * @param repositoryFactory the repository factory
     */
    public AddTaskCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        TaskService service=new TaskService(repositoryFactory);
        String title=request.getParameter(PARAMETER_TITLE);
        String description=request.getParameter(PARAMETER_DESCRIPTION);
        Object attribute = request.getSession().getAttribute(PARAMETER_COURSE_ID);
        String courseIdAttribute = String.valueOf(attribute);

        if(numberValidator.isValid(courseIdAttribute)) {
            int courseId = Integer.parseInt(String.valueOf(courseIdAttribute));
            service.addTask(new Task(title, courseId, description));
        } else {
            throw new InvalidDataException();
        }

        Command commandGetTasks=new GetTasksCommand(repositoryFactory);
        commandGetTasks.execute(request, response);

        return new CommandResult(TARGET_PAGE, true);
    }
}
