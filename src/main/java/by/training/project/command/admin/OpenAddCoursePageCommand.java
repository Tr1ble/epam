package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.repository.factory.RepositoryFactory;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Open add course page command.
 */
public class OpenAddCoursePageCommand implements AdminCommand {

    /**
     * The constant ADD_COURSE_PAGE.
     */
    private static final String ADD_COURSE_PAGE = "/WEB-INF/add-course.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Open add course page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenAddCoursePageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        return new CommandResult(ADD_COURSE_PAGE, false);
    }
}
