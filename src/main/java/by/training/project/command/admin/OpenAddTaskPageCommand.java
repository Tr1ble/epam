package by.training.project.command.admin;

import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * The type Open add task page command.
 */
public class OpenAddTaskPageCommand implements AdminCommand {

    /**
     * The constant COURSE_ID.
     */
    private static final String COURSE_ID = "courseId";
    /**
     * The constant ADD_TASK_PAGE.
     */
    private static final String ADD_TASK_PAGE = "/WEB-INF/add-task.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        request.getSession().setAttribute(COURSE_ID, request.getParameter(COURSE_ID));
        return new CommandResult(ADD_TASK_PAGE, false);
    }
}
