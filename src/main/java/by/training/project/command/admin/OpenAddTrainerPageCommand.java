package by.training.project.command.admin;

import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * The type Open add trainer page command.
 */
public class OpenAddTrainerPageCommand implements AdminCommand {

    /**
     * The constant ADD_TRAINER_PAGE.
     */
    private static final String ADD_TRAINER_PAGE = "/WEB-INF/add-trainer.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        return new CommandResult(ADD_TRAINER_PAGE, false);
    }

}
