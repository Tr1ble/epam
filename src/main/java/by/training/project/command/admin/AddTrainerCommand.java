package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.entities.Trainer;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TrainerService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Add trainer command.
 */
public class AddTrainerCommand implements AdminCommand {

    private RepositoryFactory repositoryFactory;


    private static final String TARGET_PAGE = "/trainers-page.jsp";

    private static final String PARAMETER_SURNAME = "surname";
    private static final String PARAMETER_FIRSTNAME = "firstname";
    private static final String PARAMETER_SECONDNAME = "secondname";
    private static final String USER_LOGIN = "userLogin";

    /**
     * Instantiates a new Add trainer command.
     *
     * @param repositoryFactory the repository factory
     */
    public AddTrainerCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        TrainerService service = new TrainerService(repositoryFactory);
        String surname = request.getParameter(PARAMETER_SURNAME);
        String firstname = request.getParameter(PARAMETER_FIRSTNAME);
        String secondname = request.getParameter(PARAMETER_SECONDNAME);
        String userLogin = request.getParameter(USER_LOGIN);

        service.addTrainer(new Trainer(firstname, surname, secondname, userLogin));

        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        return new CommandResult(TARGET_PAGE, true);
    }
}
