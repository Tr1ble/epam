package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.entities.Course;
import by.training.project.entities.Trainer;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CourseService;
import by.training.project.service.TrainerService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Optional;

/**
 * The type Open add course page command.
 */
public class OpenEditTrainerPageCommand implements AdminCommand {

    /**
     * The constant ADD_COURSE_PAGE.
     */
    private static final String EDIT_TRAINER_PAGE = "/WEB-INF/edit-trainer.jsp";
    private static final String TRAINER_ID = "trainerId";
    private static final String TRAINER_SURNAME = "surname";
    private static final String TRAINER_NAME = "firstname";
    private static final String TRAINER_SECNAME = "secondname";
    private static final String TRAINER_LOGIN = "userLogin";
    public static final String IS_BUSY = "isBusy";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Open add course page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenEditTrainerPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        int trainerId = Integer.parseInt(request.getParameter(TRAINER_ID));

        TrainerService trainerService = new TrainerService(repositoryFactory);
        Optional<Trainer> trainer = trainerService.getTrainerByID(trainerId);

        if (trainer.isPresent()) {
            String surname = trainer.get().getSurname();
            String firstname = trainer.get().getFirstname();
            String secname = trainer.get().getSecondname();
            String userLogin = trainer.get().getUserLogin();
            boolean isBusy = trainer.get().isBusy();
            request.setAttribute(TRAINER_ID, trainerId);
            request.setAttribute(IS_BUSY, isBusy);
            request.setAttribute(TRAINER_SURNAME, surname);
            request.setAttribute(TRAINER_NAME, firstname);
            request.setAttribute(TRAINER_SECNAME, secname);
            request.setAttribute(TRAINER_LOGIN, userLogin);
        }

        return new CommandResult(EDIT_TRAINER_PAGE, false);
    }

}
