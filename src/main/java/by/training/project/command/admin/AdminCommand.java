package by.training.project.command.admin;

import by.training.project.command.Command;

/**
 * The marker interface Admin command.
 */
public interface AdminCommand extends Command {
}
