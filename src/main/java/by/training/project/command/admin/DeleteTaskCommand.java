package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetTasksCommand;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Delete task command.
 */
public class DeleteTaskCommand implements AdminCommand {

    /**
     * The constant TASK_ID.
     */
    private static final String TASK_ID = "taskId";
    /**
     * The constant COMMAND_GET_TRAINERS.
     */
    private static final String COMMAND_GET_TRAINERS = "/training-page.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Delete task command.
     *
     * @param repositoryFactory the repository factory
     */
    public DeleteTaskCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        TaskService service=new TaskService(repositoryFactory);
        String taskId = request.getParameter(TASK_ID);
        service.removeTask(Integer.parseInt(taskId));

        Command commandGetTrainers = new GetTrainersCommand(repositoryFactory);
        commandGetTrainers.execute(request, response);

        Command commandGetTasks = new GetTasksCommand(repositoryFactory);
        commandGetTasks.execute(request, response);

        return new CommandResult(COMMAND_GET_TRAINERS, true);
    }
}
