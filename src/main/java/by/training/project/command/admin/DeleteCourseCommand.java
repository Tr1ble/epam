package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.common.GetCoursesCommand;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CourseService;
import by.training.project.service.TrainerService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Delete course command.
 */
public class DeleteCourseCommand implements AdminCommand {

    /**
     * The constant COURSE_ID.
     */
    private static final String COURSE_ID = "courseId";
    /**
     * The constant TRAINER_ID.
     */
    private static final String TRAINER_ID = "trainerId";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Delete course command.
     *
     * @param repositoryFactory the repository factory
     */
    public DeleteCourseCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        CourseService service=new CourseService(repositoryFactory);
        String courseId = request.getParameter(COURSE_ID);

        int trainerId = Integer.parseInt(request.getParameter(TRAINER_ID));
        service.removeCourse(Integer.parseInt(courseId));


        TrainerService trainerService=new TrainerService(repositoryFactory);
        trainerService.changeTrainerBusy(trainerId, false);

        Command commandGetCourses = new GetCoursesCommand(repositoryFactory);
        commandGetCourses.execute(request, response);

        return new CommandResult("/courses-page.jsp", true);
    }
}
