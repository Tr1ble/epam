package by.training.project.command.admin;

import by.training.project.command.Command;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetCoursesCommand;
import by.training.project.command.common.GetTrainersCommand;
import by.training.project.command.common.OpenCoursesPageCommand;
import by.training.project.entities.Course;
import by.training.project.entities.Trainer;
import by.training.project.exceptions.InvalidDataException;
import by.training.project.io.validator.DateValidator;
import by.training.project.io.validator.Validator;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CourseService;
import by.training.project.service.TrainerService;
import by.training.project.util.StringHelper;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * The type Add course command.
 */
public class EditCourseCommand implements AdminCommand {


    private final static Logger LOG = Logger.getLogger(EditCourseCommand.class.getName());

    private static final String PARAMETER_COURSE= "courseId";
    private static final String PARAMETER_TRAINER= "trainer";
    private static final String PARAMETER_TITLE = "title";
    private static final String PARAMETER_START_DATE = "startDate";
    private static final String PARAMETER_END_DATE = "endDate";

    private static final String DATE_PATTERN = "yyyy-MM-dd";


    private static final String TARGET_PAGE = "/courses-page.jsp";
    private static final String OLD_TRAINER = "oldTrainer";

    private RepositoryFactory repositoryFactory;
    private Validator dateValidator=new DateValidator();


    /**
     * Instantiates a new Add course command.
     *
     * @param repositoryFactory the repository factory
     */
    public EditCourseCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException,  ParseException, NoSuchAlgorithmException {

        CourseService service=new CourseService(repositoryFactory);

        String trainerData= request.getParameter(PARAMETER_TRAINER);
        StringHelper helper = new StringHelper();
        String[] fio =helper.splitStringInto3Words(trainerData);

        TrainerService trainerService=new TrainerService(repositoryFactory);

        Optional<Trainer> trainer = trainerService.getTrainerByFio(fio[0], fio[1], fio[2]);

        String title=request.getParameter(PARAMETER_TITLE);
        String parameterStartDate = request.getParameter(PARAMETER_START_DATE);
        String parameterEndDate = request.getParameter(PARAMETER_END_DATE);


        int courseId = Integer.parseInt(request.getParameter(PARAMETER_COURSE));

        if(dateValidator.isValid(parameterStartDate) || dateValidator.isValid(parameterEndDate)) {
            if(trainer.isPresent()) {
                Date startDate = new SimpleDateFormat(DATE_PATTERN).parse(parameterStartDate);
                Date endDate = new SimpleDateFormat(DATE_PATTERN).parse(parameterEndDate);
                System.out.println("a");
                Integer trainerId = trainer.get().getId();
                System.out.println(trainerId);
                service.updateCourse(new Course(courseId,trainerId, title, startDate, endDate), courseId);

                String old = request.getParameter(OLD_TRAINER);
                String[] oldFio = helper.splitStringInto3Words(old);
                Optional<Trainer> oldTrainer = trainerService.getTrainerByFio(oldFio[0], oldFio[1], oldFio[2]);
                if(oldTrainer.isPresent()){
                    trainerService.changeTrainerBusy(oldTrainer.get().getId(), false);
                }
                trainerService.changeTrainerBusy(trainerId, true);

            }
        } else {
            LOG.error("Date parsing exception");
            throw new InvalidDataException();
        }


        Command commandOpenCourse =new OpenCoursesPageCommand(repositoryFactory);
        commandOpenCourse.execute(request, response);

        return new CommandResult(TARGET_PAGE, true);
    }
}
