package by.training.project.command;

import by.training.project.command.admin.*;
import by.training.project.command.common.*;
import by.training.project.command.student.GetCurrentStudentCommand;
import by.training.project.command.student.OpenRegistrationPageCommand;
import by.training.project.command.student.RegistrationCommand;
import by.training.project.command.student.UnregisterCommand;
import by.training.project.command.trainer.GetCurrentTrainerCommand;
import by.training.project.command.trainer.OpenHistoryPageCommand;
import by.training.project.command.trainer.OpenVerifyTaskPageCommand;
import by.training.project.command.trainer.VerifyTaskCommand;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.repository.factory.RepositoryFactory;

/**
 * Factory Method for all of command need to be processed
 */


public class CommandFactory {



    private RepositoryFactory repositoryFactory;

    public CommandFactory(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    public Command getCommand(String command) throws IllegalArgumentException {
        switch (command) {
            case "login":
                return new LoginCommand(repositoryFactory);
            case "getCourses":
                return new GetCoursesCommand(repositoryFactory);
            case "getTasks":
                return new GetTasksCommand(repositoryFactory);
            case "openAddCoursePage":
                return new OpenAddCoursePageCommand(repositoryFactory);
            case "addTrainer":
                return new AddTrainerCommand(repositoryFactory);
            case "addCourse":
                return new AddCourseCommand(repositoryFactory);
            case "addTask":
                return new AddTaskCommand(repositoryFactory);
            case "getTrainers":
                return new GetTrainersCommand(repositoryFactory);
            case "openAddTrainerPage":
                return new OpenAddTrainerPageCommand();
            case "logout":
                return new LogoutCommand();
            case "openTrainersPage":
                return new OpenTrainersPageCommand(repositoryFactory);
            case "openCoursesPage":
                return new OpenCoursesPageCommand(repositoryFactory);
            case "openTrainingPage":
                return new OpenTrainingPageCommand(repositoryFactory);
            case "removeCourse":
                return new DeleteCourseCommand(repositoryFactory);
            case "removeTrainer":
                return new DeleteTrainerCommand(repositoryFactory);
            case "removeTask":
                return new DeleteTaskCommand(repositoryFactory);
            case "openAddTaskPage":
                return new OpenAddTaskPageCommand();
            case "registration":
                return new RegistrationCommand(repositoryFactory);
            case "unregister":
                return new UnregisterCommand(repositoryFactory);
            case "openRegistrationPage":
                return new OpenRegistrationPageCommand(repositoryFactory);
            case "getStudents":
                return new GetStudentsCommand(repositoryFactory);
            case "getCurrentStudent":
                return new GetCurrentStudentCommand(repositoryFactory);
            case "getCurrentTrainer":
                return new GetCurrentTrainerCommand(repositoryFactory);
            case "openVerifyPage":
                return new OpenVerifyTaskPageCommand(repositoryFactory);
            case "getCompletedTasks":
                return new GetCompletedTasksCommand(repositoryFactory);
            case "openCompletedTasksPage":
                return new GetCompletedTasksCommand(repositoryFactory);
            case "verifyTask":
                return new VerifyTaskCommand(repositoryFactory);
            case "openHistoryPage":
                return new OpenHistoryPageCommand(repositoryFactory);
            case "openEditCoursePage":
                return new OpenEditCoursePageCommand(repositoryFactory);
            case "editCourse":
                return new EditCourseCommand(repositoryFactory);
            case "openEditTrainerPage":
                return new OpenEditTrainerPageCommand(repositoryFactory);
            case "editTrainer":
                return new EditTrainerCommand(repositoryFactory);
            default:
                throw new IllegalArgumentException("There is no such a command: " + command);
        }
    }
}
