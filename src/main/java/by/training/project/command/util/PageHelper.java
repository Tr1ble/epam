package by.training.project.command.util;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.entities.Role;
import by.training.project.entities.User;


import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Page helper.
 */
public class PageHelper {

    private CommandFactory commandFactory;

    private static final String GET_COURSES = "getCourses";
    private static final String GET_CURRENT_STUDENT = "getCurrentStudent";
    private static final String GET_CURRENT_TRAINER = "getCurrentTrainer";

    private static final String USER_ATTRIBUTE = "user";

    /**
     * Instantiates a new Page helper.
     *
     * @param commandFactory the command factory
     */
    public PageHelper(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    /**
     * Update courses page.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException              the io exception
     * @throws ClassNotFoundException   the class not found exception
     * @throws ParseException           the parse exception
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public void updateCoursesPage(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        commandFactory.getCommand(GET_COURSES).execute(request, response);
        commandFactory.getCommand(GET_CURRENT_STUDENT).execute(request, response);
    }

    /**
     * Gets current student.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException              the io exception
     * @throws ClassNotFoundException   the class not found exception
     * @throws ParseException           the parse exception
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public void getCurrentStudent(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE);
        if(user.getRole().equals(Role.STUDENT)) {
            Command command = commandFactory.getCommand(GET_CURRENT_STUDENT);
            command.execute(request, response);
        }
    }

    /**
     * Gets current trainer.
     *
     * @param request  the request
     * @param response the response
     * @throws IOException              the io exception
     * @throws ClassNotFoundException   the class not found exception
     * @throws ParseException           the parse exception
     * @throws NoSuchAlgorithmException the no such algorithm exception
     */
    public void getCurrentTrainer(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        User user = (User) request.getSession().getAttribute(USER_ATTRIBUTE);
        if(user.getRole().equals(Role.TRAINER)) {
            Command command = commandFactory.getCommand(GET_CURRENT_TRAINER);
            command.execute(request, response);
        }
    }
}
