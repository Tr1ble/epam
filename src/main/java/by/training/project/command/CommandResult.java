package by.training.project.command;


/**
 * The type Command result. It needed in order to
 * determine where is user need to be sent and
 * way of it (forward or redirect).
 */
public class CommandResult {

    private String page;
    private boolean isRedirect;

    /**
     * Instantiates a new Command result.
     *
     * @param page       the page
     * @param isRedirect the is redirect
     */
    public CommandResult(String page, boolean isRedirect) {
        this.page = page;
        this.isRedirect = isRedirect;
    }

    /**
     * Gets page.
     *
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * Sets page.
     *
     * @param page the page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * Is redirect boolean.
     *
     * @return the boolean
     */
    public boolean isRedirect() {
        return isRedirect;
    }

    /**
     * Sets redirect.
     *
     * @param redirect the redirect
     */
    public void setRedirect(boolean redirect) {
        isRedirect = redirect;
    }
}
