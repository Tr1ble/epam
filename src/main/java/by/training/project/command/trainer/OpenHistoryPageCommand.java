package by.training.project.command.trainer;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetCompletedTasksCommand;
import by.training.project.command.common.GetTasksCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.CompletedTask;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CompletedTaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

/**
 * The type Open history page command.
 */
public class OpenHistoryPageCommand implements TrainerCommand {

    /**
     * The constant HISTORY_PAGE.
     */
    private static final String HISTORY_PAGE = "/history-page.jsp";
    private RepositoryFactory repositoryFactory;

    private static final String COMPLETED_TASKS_LIST = "completed-tasks-list";

    private static final String COURSE_ID = "courseId";

    /**
     * Instantiates a new Open history page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenHistoryPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException,  ParseException, NoSuchAlgorithmException {

        Command getTasksCommand=new GetTasksCommand(repositoryFactory);
        getTasksCommand.execute(request, response);

        String courseId = request.getParameter(COURSE_ID);

        CompletedTaskService service=new CompletedTaskService(repositoryFactory);
        List<CompletedTask> completedTaskList=service.getCompletedTasksByCourse(Integer.parseInt(courseId));
        request.getSession().setAttribute(COMPLETED_TASKS_LIST, completedTaskList);

        return new CommandResult(HISTORY_PAGE, false);
    }
}
