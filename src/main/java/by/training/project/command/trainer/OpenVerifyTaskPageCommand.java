package by.training.project.command.trainer;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetCompletedTasksCommand;
import by.training.project.command.common.GetStudentsCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.repository.factory.RepositoryFactory;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Open verify task page command.
 */
public class OpenVerifyTaskPageCommand implements TrainerCommand {

    /**
     * The constant TASK_ID.
     */
    private static final String TASK_ID = "taskId";
    /**
     * The constant COURSE_ID.
     */
    private static final String COURSE_ID = "courseId";
    /**
     * The constant VERIFY_TASK_PAGE.
     */
    private static final String VERIFY_TASK_PAGE = "/WEB-INF/verify-task.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Open verify task page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenVerifyTaskPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command getStudentsCommand = new GetStudentsCommand(repositoryFactory);
        getStudentsCommand.execute(request, response);

        Command getCompletedTasksCommand=new GetCompletedTasksCommand(repositoryFactory);
        getCompletedTasksCommand.execute(request, response);

        int taskId= Integer.parseInt(String.valueOf(request.getParameter(TASK_ID)));
        request.getSession().setAttribute(TASK_ID, taskId);
        int courseId= Integer.parseInt(String.valueOf(request.getParameter(COURSE_ID)));
        request.getSession().setAttribute(COURSE_ID, courseId);

        request.getSession().setAttribute(TASK_ID, request.getParameter(TASK_ID));
        return new CommandResult(VERIFY_TASK_PAGE, false);
    }
}
