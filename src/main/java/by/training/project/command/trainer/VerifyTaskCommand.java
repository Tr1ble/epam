package by.training.project.command.trainer;

import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.common.GetCompletedTasksCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.CompletedTask;
import by.training.project.entities.Student;
import by.training.project.entities.Task;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CompletedTaskService;
import by.training.project.service.StudentService;
import by.training.project.service.TaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Optional;

/**
 * The type Verify task command.
 */
public class VerifyTaskCommand implements TrainerCommand {

    /**
     * The constant TRAINING_PAGE.
     */
    private static final String TRAINING_PAGE = "/training-page.jsp";
    /**
     * The constant TASK_ID.
     */
    private static final String TASK_ID = "taskId";
    /**
     * The constant MARK.
     */
    private static final String MARK = "mark";
    /**
     * The constant FEEDBACK.
     */
    private static final String FEEDBACK = "feedback";
    /**
     * The constant STUDENT.
     */
    private static final String STUDENT = "student";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Verify task command.
     *
     * @param repositoryFactory the repository factory
     */
    public VerifyTaskCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        CompletedTaskService service = new CompletedTaskService(repositoryFactory);
        int taskId = Integer.parseInt(request.getParameter(TASK_ID));

        TaskService taskService = new TaskService(repositoryFactory);

        Optional<Task> optionalTask = taskService.findTaskById(taskId);
        if (optionalTask.isPresent()) {
            Task task=optionalTask.get();
            double mark = Double.parseDouble(request.getParameter(MARK));
            String feedback = request.getParameter(FEEDBACK);

            String studentData= request.getParameter(STUDENT);
            String[] fio = studentData.split(" ");
            String surname = fio[0];
            String firstname = fio[1];
            String secname = fio[2];

            StudentService studentService = new StudentService(repositoryFactory);
            Optional<Student> student = studentService.getTrainerByFio(surname, firstname, secname);
            if(student.isPresent()) {
                String studentLogin = student.get().getUser();
                service.addCompletedTask(new CompletedTask(taskId, task.getCourseId(), task.getDescription(), mark, feedback, studentLogin));

                new GetCompletedTasksCommand(repositoryFactory).execute(request, response);
            }


        }
        return new CommandResult(TRAINING_PAGE, true);
    }
}
