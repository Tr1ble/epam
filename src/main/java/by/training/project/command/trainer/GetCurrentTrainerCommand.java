package by.training.project.command.trainer;

import by.training.project.command.CommandResult;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.Trainer;
import by.training.project.entities.User;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TrainerService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

public class GetCurrentTrainerCommand implements TrainerCommand {
    private static final String CURRENT_TRAINER = "current-trainer";
    private static final String USER = "user";
    private static final String TRAINING_PAGE = "/training-page.jsp";
    private RepositoryFactory repositoryFactory;

    public GetCurrentTrainerCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException {
        TrainerService service=new TrainerService(repositoryFactory);
        User user = (User) request.getSession().getAttribute(USER);
        Optional<Trainer> trainer =service.findCurrentTrainerByUserLogin(user.getLogin());
        System.out.println(trainer);
        trainer.ifPresent(trainer1 -> request.getSession().setAttribute(CURRENT_TRAINER, trainer1));
        return new CommandResult(TRAINING_PAGE, false);
    }
}
