package by.training.project.command.common;

import by.training.project.command.Command;
import by.training.project.command.CommandResult;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * The type Logout command.
 */
public class LogoutCommand implements Command {

    /**
     * The constant USER.
     */
    private static final String USER = "user";
    /**
     * The constant START_PAGE.
     */
    private static final String START_PAGE = "/index.jsp";

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        request.getSession().removeAttribute(USER);
        return new CommandResult(START_PAGE, true);
    }
}
