package by.training.project.command.common;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.common.CommonCommand;
import by.training.project.command.student.GetCurrentStudentCommand;
import by.training.project.entities.Role;
import by.training.project.entities.User;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TrainerService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

public class OpenCoursesPageCommand implements CommonCommand {

    private static final String BUSY_TRAINER_LIST = "busy-trainer-list";
    private static final String USER = "user";
    private static final String COURSES_PAGE = "/courses-page.jsp";
    private RepositoryFactory repositoryFactory;

    public OpenCoursesPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command getCoursesCommand = new GetCoursesCommand(repositoryFactory);
        Command getCurrentStudentCommand = new GetCurrentStudentCommand(repositoryFactory);

        TrainerService trainerService=new TrainerService(repositoryFactory);
        request.getSession().setAttribute(BUSY_TRAINER_LIST, trainerService.getTrainerByBusy(true));

        getCoursesCommand.execute(request, response);
        User user = (User) request.getSession().getAttribute(USER);
        if(user.getRole().equals(Role.STUDENT)) {
            getCurrentStudentCommand.execute(request, response);
        }
        return new CommandResult(COURSES_PAGE, false);
    }
}
