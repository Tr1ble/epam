package by.training.project.command.common;

import by.training.project.command.Command;
import by.training.project.command.CommandResult;
import by.training.project.entities.User;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.UserService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Login command.
 */
public class LoginCommand implements Command {

    /**
     * The constant USERNAME.
     */
    private static final String USERNAME = "username";
    /**
     * The constant PASSWORD.
     */
    private static final String PASSWORD = "password";
    /**
     * The constant USER.
     */
    private static final String USER = "user";
    /**
     * The constant MAIN_PAGE.
     */
    private static final String MAIN_PAGE = "/WEB-INF/main-page.jsp";
    /**
     * The constant LANG_ATTRIBUTE.
     */
    private static final String LANG_ATTRIBUTE = "lang";
    /**
     * The constant BUNDLE_FILE_PATH.
     */
    private static final String BUNDLE_FILE_PATH = "C:\\Users\\Артем\\IdeaProjects\\webapp\\src\\main\\resources";
    /**
     * The constant BUNDLE_NAME.
     */
    private static final String BUNDLE_NAME = "content";
    private static final String ALGORITHM = "MD5";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String HOME_PAGE = "/index.jsp";
    public static final String MESSAGES_INVALID_PASS = "messages.invalid_pass";
    private RepositoryFactory factory;

    private ResourceBundle resourceBundle;


    /**
     * Instantiates a new Login command.
     *
     * @param factory the factory
     */
    public LoginCommand(RepositoryFactory factory) {
        this.factory = factory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException,  NoSuchAlgorithmException {

        UserService service=new UserService(factory);
        String login = request.getParameter(USERNAME);

        String password = request.getParameter(PASSWORD);
        String hashedPassword = getHashedPassword(password);



        Optional<User> user=service.login(login, hashedPassword);
        HttpSession session = request.getSession();
        if(user.isPresent()) {
            session.setAttribute(USER, user.get());
            return new CommandResult(MAIN_PAGE, false);
        } else {
            lokalize(session);
            request.setAttribute(ERROR_MESSAGE,resourceBundle.getString(MESSAGES_INVALID_PASS));
            return new CommandResult(HOME_PAGE, false);
        }
    }

    /**
     *
     * @param password variable is need to be hashed
     * @return hashed password
     * @throws NoSuchAlgorithmException if can't find algorithm for hashing
     */

    private String getHashedPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance(ALGORITHM);
        md.update(password.getBytes());
        byte[] bytes = md.digest();
        StringBuilder stringBuilder = new StringBuilder();
        for (byte aByte : bytes) {
            stringBuilder.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuilder.toString();
    }

    /**
     *
     * @param session where localization take place
     * @throws MalformedURLException if url is malformed
     */

    private void lokalize(HttpSession session) throws MalformedURLException {
        String lang = (String) session.getAttribute(LANG_ATTRIBUTE);
        Locale locale = new Locale(lang);
        File file = new File(BUNDLE_FILE_PATH);
        URL[] urls = {file.toURI().toURL()};
        ClassLoader loader = new URLClassLoader(urls);
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME, locale,getClass().getClassLoader());

    }
}
