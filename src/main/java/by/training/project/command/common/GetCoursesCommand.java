package by.training.project.command.common;

import by.training.project.command.CommandResult;
import by.training.project.command.common.CommonCommand;
import by.training.project.entities.Course;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CourseService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class GetCoursesCommand implements CommonCommand {

    private static final String COURSES_LIST = "courses-list";
    private static final String COURSES_PAGE_JSP = "/courses-page.jsp";
    private RepositoryFactory factory;


    /**
     * Instantiates a new Get courses command.
     *
     * @param factory the factory
     */
    public GetCoursesCommand(RepositoryFactory factory) {
        this.factory = factory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        CourseService service=new CourseService(factory);
        List<Course> courseList=service.getAllCourses();
        request.getSession().setAttribute(COURSES_LIST, courseList);
        return new CommandResult(COURSES_PAGE_JSP, false);
    }
}
