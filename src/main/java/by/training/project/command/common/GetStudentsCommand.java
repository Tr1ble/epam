package by.training.project.command.common;

import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.Student;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.StudentService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * The type Get students command.
 */
public class GetStudentsCommand implements AdminCommand, TrainerCommand {
    /**
     * The constant STUDENTS_LIST.
     */
    private static final String STUDENTS_LIST = "students-list";
    /**
     * The constant COURSES_PAGE_JSP.
     */
    private static final String COURSES_PAGE_JSP = "/courses-page.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Get students command.
     *
     * @param repositoryFactory the repository factory
     */
    public GetStudentsCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,ClassNotFoundException {
        StudentService service=new StudentService(repositoryFactory);
        List<Student> studentList=service.getAllStudents();
        request.getSession().setAttribute(STUDENTS_LIST, studentList);
        return new CommandResult(COURSES_PAGE_JSP, false);
    }
}
