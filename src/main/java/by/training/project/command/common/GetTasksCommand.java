package by.training.project.command.common;

import by.training.project.command.CommandResult;
import by.training.project.command.common.CommonCommand;
import by.training.project.entities.Task;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * The type Get tasks command.
 */
public class GetTasksCommand implements CommonCommand {

    /**
     * The constant TASKS_LIST.
     */
    private static final String TASKS_LIST = "tasks-list";
    /**
     * The constant TRAINING_PAGE_JSP.
     */
    private static final String TRAINING_PAGE_JSP = "/training-page.jsp";
    private RepositoryFactory factory;


    /**
     * Instantiates a new Get tasks command.
     *
     * @param factory the factory
     */
    public GetTasksCommand(RepositoryFactory factory) {
        this.factory = factory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        TaskService service=new TaskService(factory);
        List<Task> taskList=service.getAllTasks();
        request.getSession().setAttribute(TASKS_LIST, taskList);
        return new CommandResult(TRAINING_PAGE_JSP, false);
    }
}
