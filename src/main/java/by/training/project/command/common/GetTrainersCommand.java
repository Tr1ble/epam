package by.training.project.command.common;

import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.entities.Trainer;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.TrainerService;
import java.util.*;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class GetTrainersCommand implements AdminCommand, TrainerCommand {
    private static final String TRAINERS_LIST = "trainers-list";
    private static final String TRAINERS_PAGE_JSP = "/trainers-page.jsp";
    private RepositoryFactory repositoryFactory;

    public GetTrainersCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        TrainerService service=new TrainerService(repositoryFactory);
        List<Trainer> trainerList=service.getAllTrainers();
        request.getSession().setAttribute(TRAINERS_LIST, trainerList);
        return new CommandResult(TRAINERS_PAGE_JSP, false);
    }
}
