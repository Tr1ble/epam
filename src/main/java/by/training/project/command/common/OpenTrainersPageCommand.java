package by.training.project.command.common;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.admin.AdminCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.repository.factory.RepositoryFactory;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

public class OpenTrainersPageCommand implements AdminCommand, TrainerCommand {

    private static final String TRAINERS_PAGE = "/trainers-page.jsp";
    private RepositoryFactory repositoryFactory;

    public OpenTrainersPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        Command getTrainersCommand=new GetTrainersCommand(repositoryFactory);
        getTrainersCommand.execute(request, response);

        Command getCoursesCommand=new GetCoursesCommand(repositoryFactory);
        getCoursesCommand.execute(request, response);

        return new CommandResult(TRAINERS_PAGE, false);
    }
}
