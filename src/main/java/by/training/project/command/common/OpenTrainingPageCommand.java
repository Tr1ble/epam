package by.training.project.command.common;

import by.training.project.command.Command;
import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.common.CommonCommand;
import by.training.project.command.util.PageHelper;
import by.training.project.repository.factory.RepositoryFactory;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

public class OpenTrainingPageCommand implements CommonCommand {

    private RepositoryFactory repositoryFactory;

    public OpenTrainingPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException,  ParseException, NoSuchAlgorithmException {
        Command getTasksCommand=new GetTasksCommand(repositoryFactory);
        getTasksCommand.execute(request, response);

        Command getCoursesCommand=new GetCoursesCommand(repositoryFactory);
        getCoursesCommand.execute(request, response);

        Command getCompletedTasksCommand =new GetCompletedTasksCommand(repositoryFactory);
        getCompletedTasksCommand.execute(request, response);

        PageHelper pageHelper=new PageHelper(new CommandFactory(repositoryFactory));
        pageHelper.getCurrentStudent(request, response);
        pageHelper.getCurrentTrainer(request, response);

        return new CommandResult("/training-page.jsp", false);
    }
}
