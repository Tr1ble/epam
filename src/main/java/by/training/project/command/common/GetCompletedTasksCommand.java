package by.training.project.command.common;

import by.training.project.command.CommandResult;
import by.training.project.command.common.CommonCommand;
import by.training.project.entities.CompletedTask;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.CompletedTaskService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * The type Get completed tasks command.
 */
public class GetCompletedTasksCommand implements CommonCommand {

    /**
     * The constant TRAINING_PAGE_JSP.
     */
    private static final String TRAINING_PAGE_JSP = "/training-page.jsp";
    /**
     * The constant COMPLETED_TASKS_LIST.
     */
    private static final String COMPLETED_TASKS_LIST = "completed-tasks-list";
    private RepositoryFactory factory;


    /**
     * Instantiates a new Get completed tasks command.
     *
     * @param factory the factory
     */
    public GetCompletedTasksCommand(RepositoryFactory factory) {
        this.factory = factory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException,  ClassNotFoundException {
        CompletedTaskService service=new CompletedTaskService(factory);
        List<CompletedTask> completedTaskList=service.getAllCompletedTasks();
        request.getSession().setAttribute(COMPLETED_TASKS_LIST, completedTaskList);
        return new CommandResult(TRAINING_PAGE_JSP, false);
    }
}
