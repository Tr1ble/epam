package by.training.project.command.common;

import by.training.project.command.student.StudentCommand;
import by.training.project.command.trainer.TrainerCommand;
import by.training.project.command.admin.AdminCommand;

/**
 * The marker interface Common command.
 */
public interface CommonCommand extends AdminCommand, TrainerCommand, StudentCommand {
}
