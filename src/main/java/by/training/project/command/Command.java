package by.training.project.command;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 *  Functional interface for all command came on
 * Front Controller. Gives an interface to execute all
 * of them despite what command it is.
 */
public interface Command {

    /**
     *
     * @param request request variable
     * @param response response variable
     * @return CommandResult object that has 2 fields: target page and is forward or no
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws ParseException
     */
    CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException;
}
