package by.training.project.command.student;

import by.training.project.command.CommandResult;
import by.training.project.command.student.StudentCommand;
import by.training.project.entities.Student;
import by.training.project.entities.User;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.StudentService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Optional;

/**
 * The type Get current student command.
 */
public class GetCurrentStudentCommand implements StudentCommand {
    /**
     * The constant COURSES_PAGE.
     */
    private static final String COURSES_PAGE= "/courses-page.jsp";
    /**
     * The constant CURRENT_STUDENT.
     */
    private static final String CURRENT_STUDENT = "current-student";
    /**
     * The constant USER.
     */
    private static final String USER = "user";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Get current student command.
     *
     * @param repositoryFactory the repository factory
     */
    public GetCurrentStudentCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException {
        StudentService service=new StudentService(repositoryFactory);
        User user = (User) request.getSession().getAttribute(USER);
        Optional<Student> student =service.findCurrentStudentByLogin(user.getLogin());
        student.ifPresent(student1 -> request.getSession().setAttribute(CURRENT_STUDENT, student1));
        if(!student.isPresent()) {
            request.getSession().setAttribute(CURRENT_STUDENT, null);
        }
        return new CommandResult(COURSES_PAGE, false);
    }
}
