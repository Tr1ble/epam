package by.training.project.command.student;

import by.training.project.command.Command;

/**
 * The marker interface Student command.
 */
public interface StudentCommand extends Command {
}
