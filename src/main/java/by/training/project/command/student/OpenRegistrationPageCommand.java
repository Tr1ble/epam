package by.training.project.command.student;

import by.training.project.command.CommandResult;
import by.training.project.command.student.StudentCommand;
import by.training.project.repository.factory.RepositoryFactory;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Open registration page command.
 */
public class OpenRegistrationPageCommand implements StudentCommand {

    /**
     * The constant COURSE_ID.
     */
    private static final String COURSE_ID = "courseId";
    /**
     * The constant REGISTRATION_JSP.
     */
    private static final String REGISTRATION_JSP = "/WEB-INF/registration.jsp";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Open registration page command.
     *
     * @param repositoryFactory the repository factory
     */
    public OpenRegistrationPageCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException {
        request.getSession().setAttribute(COURSE_ID, request.getParameter(COURSE_ID));
        return new CommandResult(REGISTRATION_JSP, false);
    }
}
