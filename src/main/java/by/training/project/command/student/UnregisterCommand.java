package by.training.project.command.student;

import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.student.StudentCommand;
import by.training.project.command.util.PageHelper;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.StudentService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

public class UnregisterCommand implements StudentCommand {

    private static final String CURRENT_STUDENT = "current-student";
    private static final String COURSE_ID = "courseId";
    private static final String COURSES_PAGE = "/courses-page.jsp";
    private RepositoryFactory repositoryFactory;

    public UnregisterCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        StudentService service=new StudentService(repositoryFactory);
        String courseId = request.getParameter(COURSE_ID);
        service.removeStudentByCourseId(Integer.parseInt(courseId));
        request.getSession().setAttribute(CURRENT_STUDENT, null);

        CommandFactory commandFactory=new CommandFactory(repositoryFactory);
        new PageHelper(commandFactory).updateCoursesPage(request, response);

        return new CommandResult(COURSES_PAGE, true);
    }
}
