package by.training.project.command.student;

import by.training.project.command.CommandFactory;
import by.training.project.command.CommandResult;
import by.training.project.command.student.StudentCommand;
import by.training.project.command.util.PageHelper;
import by.training.project.entities.Student;
import by.training.project.repository.factory.RepositoryFactory;
import by.training.project.service.StudentService;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * The type Registration command.
 */
public class RegistrationCommand implements StudentCommand {

    /**
     * The constant COURSES_PAGE.
     */
    private static final String COURSES_PAGE = "/courses-page.jsp";
    /**
     * The constant SURNAME.
     */
    private static final String SURNAME = "surname";
    /**
     * The constant FIRSTNAME.
     */
    private static final String FIRSTNAME = "firstname";
    /**
     * The constant SECONDNAME.
     */
    private static final String SECONDNAME = "secondname";
    /**
     * The constant COURSE_ID.
     */
    private static final String COURSE_ID = "courseId";
    /**
     * The constant USER.
     */
    private static final String USER = "user";
    private RepositoryFactory repositoryFactory;

    /**
     * Instantiates a new Registration command.
     *
     * @param repositoryFactory the repository factory
     */
    public RegistrationCommand(RepositoryFactory repositoryFactory) {
        this.repositoryFactory = repositoryFactory;
    }

    @Override
    public CommandResult execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ClassNotFoundException, ParseException, NoSuchAlgorithmException {
        StudentService service=new StudentService (repositoryFactory);
        String surname=request.getParameter(SURNAME);
        String firstname=request.getParameter(FIRSTNAME);
        String secondname=request.getParameter(SECONDNAME);
        int courseId=Integer.parseInt(String.valueOf(request.getSession().getAttribute(COURSE_ID)));
        String userLogin=request.getParameter(USER);
        service.register(new Student(firstname, surname, secondname, userLogin, courseId));

        CommandFactory commandFactory=new CommandFactory(repositoryFactory);
        new PageHelper(commandFactory).updateCoursesPage(request, response);

        return new CommandResult(COURSES_PAGE, true);
    }
}
