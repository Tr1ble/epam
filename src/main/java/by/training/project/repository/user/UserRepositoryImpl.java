package by.training.project.repository.user;

import by.training.project.builder.UserBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.Trainer;
import by.training.project.entities.User;
import by.training.project.entities.enums.TrainerEnum;
import by.training.project.entities.enums.UserEnum;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;
import by.training.project.repository.user.UserRepository;

import javax.naming.NamingException;
import java.util.LinkedHashMap;
import java.util.List;
import java.sql.SQLException;
import java.util.Optional;

public class UserRepositoryImpl extends AbstractRepository<User> implements UserRepository {

    private static final String FIND_USER= "SELECT * FROM user ";
    private static final String ADD_USER = "INSERT INTO student (`login`,`password`, 'role') VALUES (?, ?, ?)";
    private static final String REMOVE_USER= "DELETE FROM user ";
    private static final String UPDATE_USER = "UPDATE course SET login=?, password=? ";

    private ProxyConnection connection;

    @Override
    public Optional<User> findUser(Specification<User> specification) throws RepositoryException {
        return executeQueryForSingleResult(FIND_USER+specification.specify(), new UserBuilder());
    }

    public UserRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(User user) throws RepositoryException {
      executeAdd(ADD_USER, getValues(user));
    }

    @Override
    public void remove(Specification<User> specification) throws RepositoryException {
        executeRemove(REMOVE_USER, specification.specify());
    }

    @Override
    public void update(User user, Specification<User> specification) throws RepositoryException {
       executeUpdate(UPDATE_USER, getValues(user), specification.specify());
    }

    @Override
    public List<User> getAll() throws RepositoryException {
        return executeQuery(FIND_USER, new UserBuilder());
    }

    private LinkedHashMap<String, Object> getValues(User user) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put(UserEnum.LOGIN.getValue(), user.getLogin());
        map.put(UserEnum.PASSWORD.getValue(), user.getPassword());
        map.put(UserEnum.ROLE.getValue(), user.getRole().getValue());
        return map;
    }

}
