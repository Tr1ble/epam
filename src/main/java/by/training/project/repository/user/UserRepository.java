package by.training.project.repository.user;


import by.training.project.entities.User;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public interface UserRepository extends Repository<User> {
    Optional<User> findUser(Specification<User> specification) throws SQLException, ClassNotFoundException, NamingException, RepositoryException;
}
