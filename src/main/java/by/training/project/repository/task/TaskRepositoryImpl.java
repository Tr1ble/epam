package by.training.project.repository.task;

import by.training.project.builder.TaskBuilder;
import by.training.project.builder.TrainersBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.CompletedTask;
import by.training.project.entities.Task;
import by.training.project.entities.enums.TaskEnum;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

public class TaskRepositoryImpl extends AbstractRepository<Task> implements TaskRepository {

    private static final String FIND_TASK = "SELECT * FROM training.task ";
    private static final String ADD_TASK = "INSERT INTO training.task (`course_id`,`title`, `task_description`) VALUES (?, ?, ?)";
    private static final String REMOVE_TASK = "DELETE FROM training.task ";

    private ProxyConnection connection;

    public TaskRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(Task task) throws RepositoryException {
        executeAdd(ADD_TASK, getValues(task));
    }

    @Override
    public void remove(Specification<Task> specification) throws RepositoryException {
        executeRemove(REMOVE_TASK,specification.specify());
    }

    @Override
    public void update(Task task, Specification<Task> specification) {

    }

    @Override
    public List<Task> getAll() throws RepositoryException {
        return executeQuery(FIND_TASK, new TaskBuilder());
    }

    @Override
    public Optional<Task> findTaskById(Specification<Task> specification) throws  RepositoryException {
        return executeQueryForSingleResult(FIND_TASK + specification.specify(), new TaskBuilder());
    }

    private LinkedHashMap<String, Object> getValues(Task task) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put(TaskEnum.COURSE_ID.getValue(), task.getCourseId());
        map.put(TaskEnum.TITLE.getValue(), task.getTitle());
        map.put(TaskEnum.DESCRIPTION.getValue(), task.getDescription());
        return map;
    }
}
