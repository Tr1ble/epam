package by.training.project.repository.task;

import by.training.project.entities.CompletedTask;
import by.training.project.entities.Task;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public interface TaskRepository extends Repository<Task> {
    Optional<Task> findTaskById(Specification<Task> specification) throws SQLException, NamingException, RepositoryException;
}
