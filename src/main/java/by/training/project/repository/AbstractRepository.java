package by.training.project.repository;

import by.training.project.builder.Builder;
import by.training.project.database.ConnectionPool;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.Identifable;
import by.training.project.exceptions.RepositoryException;
import org.apache.log4j.Logger;

import javax.naming.NamingException;
import java.security.SignedObject;
import java.util.*;

import java.sql.*;

/**
 * The type Abstract repository. It's responsible for
 * working with database.
 *
 * @param <T> the type parameter
 */
public abstract class AbstractRepository<T extends Identifable> implements Repository<T> {

    private static final int FIRST_VALUE_NUMBER = 1;
    private ProxyConnection connection;
    private PreparedStatement preparedStatement;
    private final static Logger LOG = Logger.getLogger(AbstractRepository.class.getName());

    /**
     * Instantiates a new Abstract repository.
     */
    public AbstractRepository() {
    }

    /**
     * Instantiates a new Abstract repository.
     *
     * @param connection the connection
     */
    public AbstractRepository(ProxyConnection connection) {
        this.connection = connection;
    }

    /**
     * Execute query list.
     *
     * @param query   the query
     * @param builder the builder
     * @param params  the params
     * @return the list
     * @throws RepositoryException the repository exception
     */
    protected List<T> executeQuery(String query, Builder<T> builder, String... params) throws RepositoryException {
        List<T> entities = new ArrayList<>(5);
        try {
            preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                T entity = builder.build(resultSet);
                entities.add(entity);
            }
            return entities;
        } catch (SQLException e) {
            LOG.error("SQL query exception");
            throw new RepositoryException();
        }
    }

    /**
     * Execute query for single result optional.
     *
     * @param query   the query
     * @param builder the builder
     * @param params  the params
     * @return the optional
     * @throws RepositoryException the repository exception
     */
    protected Optional<T> executeQueryForSingleResult(String query, Builder<T> builder, String... params) throws RepositoryException {
        List<T> entities = executeQuery(query, builder, params);
        if (entities.size() >= 1) {
            return Optional.of(entities.get(0));
        } else {
            return Optional.empty();
        }
    }

    /**
     * Execute add int.
     *
     * @param action the action
     * @param values the values
     * @return the int
     * @throws RepositoryException the repository exception
     */
    protected int executeAdd(String action, LinkedHashMap<String, Object> values) throws RepositoryException {
        try {
            System.out.println(action + values);
            preparedStatement = connection.prepareStatement(action);
            setValues(preparedStatement, values);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.error("SQL adding exception");
            throw new RepositoryException("Add executing exception");
        }
    }

    /**
     * Execute remove int.
     *
     * @param action the action
     * @param params the params
     * @return the int
     * @throws RepositoryException the repository exception
     */
    protected int executeRemove(String action, String params) throws RepositoryException {
        try {
            System.out.println(action + params);
            preparedStatement = connection.prepareStatement(action + params);
            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.error("SQL deleting exception");
            throw new RepositoryException("Remove executing exception");
        }
    }

    /**
     * Execute update int.
     *
     * @param action the action
     * @param values the values
     * @param params the params
     * @return the int
     * @throws RepositoryException the repository exception
     */
    protected int executeUpdate(String action, LinkedHashMap<String, Object> values, String params) throws RepositoryException {
        try {
            preparedStatement = connection.prepareStatement(action + params);
            setValues(preparedStatement, values);

            return preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.error("SQL updating exception");
            throw new RepositoryException("Update executing exception");
        }
    }
    private void setValues(PreparedStatement preparedStatement, LinkedHashMap<String, Object> values) throws SQLException {
        int i = FIRST_VALUE_NUMBER;
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            preparedStatement.setObject(i, entry.getValue());
            System.out.println(entry.getKey() + entry.getValue());
            i++;
        }
    }
}
