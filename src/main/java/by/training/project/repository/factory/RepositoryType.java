package by.training.project.repository.factory;

public enum RepositoryType {
    USER,
    COURSE,
    TASK,
    STUDENT,
    TRAINER,
    COMPLETED_TASK
}
