package by.training.project.repository.factory;

import by.training.project.database.ProxyConnection;
import by.training.project.exceptions.IllegalArgumentException;
import by.training.project.repository.Repository;
import by.training.project.repository.completedtask.CompletedTaskRepositoryImpl;
import by.training.project.repository.course.CourseRepositoryImpl;
import by.training.project.repository.student.StudentRepositoryImpl;
import by.training.project.repository.task.TaskRepositoryImpl;
import by.training.project.repository.trainer.TrainerRepositoryImpl;
import by.training.project.repository.user.UserRepositoryImpl;

public class RepositoryFactory {
    private ProxyConnection proxyConnection;

    public RepositoryFactory() {
    }

    public RepositoryFactory(ProxyConnection proxyConnection) {
        this.proxyConnection = proxyConnection;
    }

    public Repository create(RepositoryType repositoryType) throws IllegalArgumentException {
        switch (repositoryType) {
            case USER:
                return new UserRepositoryImpl(proxyConnection);
            case COURSE:
                return new CourseRepositoryImpl(proxyConnection);
            case TRAINER:
                return new TrainerRepositoryImpl(proxyConnection);
            case TASK:
                return new TaskRepositoryImpl(proxyConnection);
            case STUDENT:
                return new StudentRepositoryImpl(proxyConnection);
            case COMPLETED_TASK:
                return new CompletedTaskRepositoryImpl(proxyConnection);
            default:
                throw new IllegalArgumentException();
        }
    }
}
