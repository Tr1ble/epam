package by.training.project.repository.trainer;

import by.training.project.builder.TrainersBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.Trainer;
import by.training.project.entities.enums.TrainerEnum;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Optional;

public class TrainerRepositoryImpl extends AbstractRepository<Trainer> implements TrainerRepository {

    private static final String FIND_TRAINER = "SELECT * FROM training.trainer ";
    private static final String ADD_TRAINER = "INSERT INTO training.trainer (`surname`, `firstname`, `secondname`,`isBusy` , `user_login`) VALUES (?, ?, ?, ?, ?)";
    private static final String REMOVE_TRAINER = "DELETE FROM training.trainer ";
    private static final String UPDATE_TRAINER = "UPDATE training.trainer SET surname=?, firstname=?, secondname=?, isBusy=?, user_login=? ";

    private ProxyConnection connection;

    @Override
    public Optional<Trainer> findTrainer(Specification<Trainer> specification) throws RepositoryException {
        return executeQueryForSingleResult(FIND_TRAINER+specification.specify(), new TrainersBuilder());
    }

    public TrainerRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(Trainer trainer) throws  RepositoryException {
        executeAdd(ADD_TRAINER, getValues(trainer));
    }

    @Override
    public void remove(Specification<Trainer> specification) throws  RepositoryException {
        executeRemove(REMOVE_TRAINER, specification.specify());
    }

    @Override
    public void update(Trainer trainer, Specification<Trainer> specification) throws  RepositoryException {
        executeUpdate(UPDATE_TRAINER , getValues(trainer) , specification.specify());
    }

    @Override
    public List<Trainer> getAll() throws RepositoryException {
        return executeQuery(FIND_TRAINER, new TrainersBuilder());
    }


    private LinkedHashMap<String, Object> getValues(Trainer trainer) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put(TrainerEnum.SURNAME.getValue(), trainer.getSurname());
        map.put(TrainerEnum.FIRSTNAME.getValue(), trainer.getFirstname());
        map.put(TrainerEnum.SECONDNAME.getValue(), trainer.getSecondname());
        map.put(TrainerEnum.ISBUSY.getValue(), String.valueOf(trainer.isBusy() ? 1 : 0));
        map.put(TrainerEnum.USER_LOGIN.getValue(), String.valueOf(trainer.getUserLogin()));
        return map;
    }


    public List<Trainer> findTrainers(Specification specification) throws RepositoryException {
        return executeQuery(FIND_TRAINER + specification.specify(), new TrainersBuilder());
    }
}
