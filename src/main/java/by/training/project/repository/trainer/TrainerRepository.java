package by.training.project.repository.trainer;


import by.training.project.entities.Trainer;
import by.training.project.entities.User;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public interface TrainerRepository extends Repository<Trainer> {
    Optional<Trainer> findTrainer(Specification<Trainer> specification) throws SQLException, ClassNotFoundException, NamingException, RepositoryException;
}
