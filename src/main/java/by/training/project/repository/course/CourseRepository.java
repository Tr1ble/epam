package by.training.project.repository.course;

import by.training.project.entities.Course;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public interface CourseRepository extends Repository<Course> {
    Optional<Course> findCourse(Specification<Course> specification) throws SQLException, NamingException, RepositoryException;
}
