package by.training.project.repository.course;

import by.training.project.builder.CourseBuilder;
import by.training.project.builder.UserBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.Course;
import by.training.project.entities.User;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CourseRepositoryImpl extends AbstractRepository<Course> implements CourseRepository {

    private static final String FIND_COURSE= "SELECT * FROM training.course ";
    private static final String ADD_COURSE = "INSERT INTO training.course (`trainer_id`, `title`, `start_date`, `end_date`) VALUES (?, ?, ?, ?) ";
    private static final String REMOVE_COURSE= "DELETE FROM training.course";
    private static final String UPDATE_COURSE = "UPDATE course SET trainer_id=?, title=?, start_date=?, end_date=? ";
    private static final String DATE_PATTERN = "yyyy.MM.dd";

    private ProxyConnection connection;

    private List<Course> courses;

    @Override
    public Optional<Course> findCourse(Specification<Course> specification) throws  RepositoryException {
        return executeQueryForSingleResult(FIND_COURSE+specification.specify(), new CourseBuilder());
    }

    public CourseRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(Course course) throws RepositoryException {
        executeAdd(ADD_COURSE, getValues(course));
    }

    @Override
    public void remove(Specification<Course> specification) throws RepositoryException {
        executeRemove(REMOVE_COURSE , specification.specify());
    }

    @Override
    public void update(Course course, Specification<Course> specification) throws RepositoryException {
        executeUpdate(UPDATE_COURSE, getValues(course), specification.specify());
    }

    @Override
    public List<Course> getAll() throws RepositoryException {
        return executeQuery(FIND_COURSE, new CourseBuilder());
    }

    //LinkedMap<Column, Value>
    protected LinkedHashMap<String, Object> getValues(Course course){
        LinkedHashMap<String, Object> values = new LinkedHashMap<>(5);
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_PATTERN);
        values.put("trainer_id", course.getTrainerId());
        values.put("title", course.getTitle());
        values.put("start_date", formatter.format(course.getStartDate()));
        values.put("end_date", formatter.format(course.getEndDate()));
        return  values;
    }
}
