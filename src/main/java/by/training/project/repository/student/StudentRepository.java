package by.training.project.repository.student;

import by.training.project.entities.Course;
import by.training.project.entities.Student;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.Optional;

public interface StudentRepository extends Repository<Student> {
    Optional<Student> findStudent(Specification<Student> specification) throws SQLException, NamingException, RepositoryException;
}
