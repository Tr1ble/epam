package by.training.project.repository.student;

import by.training.project.builder.CourseBuilder;
import by.training.project.builder.StudentBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.Course;
import by.training.project.entities.Student;
import by.training.project.entities.enums.StudentEnum;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class StudentRepositoryImpl extends AbstractRepository<Student> implements StudentRepository {

    private static final String FIND_STUDENT= "SELECT * FROM training.student ";
    private static final String ADD_STUDENT = "INSERT INTO training.student (`surname`, `firstname`,`secondname`, `course_id`, `user_login`) VALUES (?, ?, ?, ?, ?)";
    private static final String REMOVE_STUDENT= "DELETE FROM training.student ";
    private static final String UPDATE_STUDENT = "UPDATE course SET trainer_id=?, ";

    private ProxyConnection connection;


    @Override
    public Optional<Student> findStudent(Specification<Student> specification) throws RepositoryException {
        return executeQueryForSingleResult(FIND_STUDENT +specification.specify(), new StudentBuilder());
    }

    public StudentRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(Student student) throws RepositoryException {
       executeAdd(ADD_STUDENT, getValues(student));
    }

    @Override
    public void remove(Specification<Student> specification) throws RepositoryException {
       executeRemove(REMOVE_STUDENT, specification.specify());
    }

    @Override
    public void update(Student student, Specification<Student> specification) throws RepositoryException {
        executeUpdate(UPDATE_STUDENT, getValues(student), specification.specify());
    }

    @Override
    public List<Student> getAll() throws  RepositoryException {
        return executeQuery(FIND_STUDENT, new StudentBuilder());
    }

    private LinkedHashMap<String, Object> getValues(Student student) {
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        map.put(StudentEnum.SURNAME.getValue(), student.getSurname());
        map.put(StudentEnum.FIRSTNAME.getValue(), student.getFirstname());
        map.put(StudentEnum.SECONDNAME.getValue(), student.getSecondname());
        map.put(StudentEnum.COURSE_ID.getValue(), student.getCourseId());
        map.put(StudentEnum.USER_LOGIN.getValue(), student.getUser());
        return map;
    }
}
