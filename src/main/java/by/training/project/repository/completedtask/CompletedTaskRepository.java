package by.training.project.repository.completedtask;

import by.training.project.entities.CompletedTask;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.Repository;
import by.training.project.repository.specification.Specification;

import java.util.List;

public interface CompletedTaskRepository extends Repository<CompletedTask> {
    List<CompletedTask> findCompletedTasks(Specification specification) throws RepositoryException;
}
