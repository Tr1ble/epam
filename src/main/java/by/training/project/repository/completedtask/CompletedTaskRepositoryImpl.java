package by.training.project.repository.completedtask;

import by.training.project.builder.CompletedTaskBuilder;
import by.training.project.database.ProxyConnection;
import by.training.project.entities.CompletedTask;
import by.training.project.entities.enums.CompletedTaskEnum;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.AbstractRepository;
import by.training.project.repository.specification.Specification;

import java.util.LinkedHashMap;
import java.util.List;

public class CompletedTaskRepositoryImpl extends AbstractRepository<CompletedTask> implements CompletedTaskRepository {

    private static final String FIND_COMPLETED_TASK = "SELECT * FROM training.completed_task ";
    private static final String ADD_COMPLETED_TASK = "INSERT INTO training.completed_task (`task_id`, `mark`, `feedback`, `student`) VALUES (?, ?, ?, ?)";
    private static final String REMOVE_COMPLETED_TASK = "DELETE FROM training.completed_task ";
    private static final String UPDATE_COMPLETED_TASK = "UPDATE training.completed_task SET task_id=?, mark=?, feedback=?, student_id=?";

    private ProxyConnection connection;

    public CompletedTaskRepositoryImpl(ProxyConnection connection) {
        super(connection);
        this.connection = connection;
    }

    @Override
    public void add(CompletedTask completedTask) throws RepositoryException {
        executeAdd(ADD_COMPLETED_TASK, getValues(completedTask));
    }

    @Override
    public void remove(Specification<CompletedTask> specification) throws RepositoryException {
        executeRemove(REMOVE_COMPLETED_TASK , specification.specify());
    }

    @Override
    public void update(CompletedTask completedTask, Specification<CompletedTask> specification) throws RepositoryException {
       executeUpdate(UPDATE_COMPLETED_TASK, getValues(completedTask), specification.specify());
    }

    @Override
    public List<CompletedTask> getAll() throws RepositoryException {
        return executeQuery(FIND_COMPLETED_TASK, new CompletedTaskBuilder());
    }

    @Override
    public List<CompletedTask> findCompletedTasks(Specification specification) throws RepositoryException {
        return executeQuery(FIND_COMPLETED_TASK + specification.specify(), new CompletedTaskBuilder());
    }

    private LinkedHashMap<String, Object> getValues(CompletedTask completedTask) {
        LinkedHashMap<String, Object > map = new LinkedHashMap<>();
        map.put(CompletedTaskEnum.TASK_ID.getValue(), String.valueOf(completedTask.getId()));
        map.put(CompletedTaskEnum.MARK.getValue(), String.valueOf(completedTask.getMark()));
        map.put(CompletedTaskEnum.FEEDBACK.getValue(), completedTask.getFeedback());
        map.put(CompletedTaskEnum.STUDENT.getValue(), completedTask.getStudent());
        return map;
    }
}
