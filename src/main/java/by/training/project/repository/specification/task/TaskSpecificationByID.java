package by.training.project.repository.specification.task;

import by.training.project.repository.specification.course.CourseSpecification;

public class TaskSpecificationByID implements TaskSpecification {
    private int id;

    public TaskSpecificationByID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (id=" + '"' + id+ '"' + ");";
    }
}
