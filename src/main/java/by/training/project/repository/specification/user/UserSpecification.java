package by.training.project.repository.specification.user;

import by.training.project.entities.User;
import by.training.project.repository.specification.Specification;

public interface UserSpecification extends Specification<User> {
}
