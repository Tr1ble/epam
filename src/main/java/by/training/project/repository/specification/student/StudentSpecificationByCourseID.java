package by.training.project.repository.specification.student;

public class StudentSpecificationByCourseID implements StudentSpecification {
    private int id;

    public StudentSpecificationByCourseID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (course_id=" + '"' + id+ '"' + ");";
    }
}
