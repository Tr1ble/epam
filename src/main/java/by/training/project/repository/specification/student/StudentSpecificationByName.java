package by.training.project.repository.specification.student;

import by.training.project.repository.specification.trainer.TrainerSpecification;

public class StudentSpecificationByName implements StudentSpecification {
    private String surname;
    private String firstname;
    private String secname;

    public StudentSpecificationByName(String surname, String firstname, String secname) {
        this.surname = surname;
        this.firstname = firstname;
        this.secname = secname;
    }

    @Override
    public String specify() {
        return " WHERE (surname=" + '"' + surname + '"'
                + " AND firstname = " + '"' + firstname + '"'
                + " AND secondname = " + '"' + secname + '"'  + ");";
    }
}
