package by.training.project.repository.specification.completedtask;

import by.training.project.repository.specification.task.TaskSpecification;

public class CompletedTaskSpecificationByID implements CompletedTaskSpecification {
    private int id;

    public CompletedTaskSpecificationByID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (id=" + '"' + id+ '"' + ");";
    }
}
