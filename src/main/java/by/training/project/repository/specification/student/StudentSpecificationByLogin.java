package by.training.project.repository.specification.student;

public class StudentSpecificationByLogin implements StudentSpecification {
    private String login;

    public StudentSpecificationByLogin(String login) {
        this.login =login;
    }

    @Override
    public String specify() {
        return " WHERE (user_login=" + '"' + login + '"' + ");";
    }
}
