package by.training.project.repository.specification.trainer;

public class TrainerSpecificationByBusy implements TrainerSpecification {
    private boolean isBusy;

    public TrainerSpecificationByBusy(boolean isBusy) {
        this.isBusy = isBusy;
    }

    @Override
    public String specify() {
        return " WHERE (isBusy=" + '"' + (isBusy ? 1:0)  + '"' + ");";
    }

}
