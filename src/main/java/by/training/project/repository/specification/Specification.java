package by.training.project.repository.specification;

import by.training.project.entities.Identifable;

public interface Specification<T extends Identifable> {
    String specify();
}