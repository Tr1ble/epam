package by.training.project.repository.specification.course;

public class CourseSpecificationByID implements CourseSpecification {
    private int id;

    public CourseSpecificationByID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (id=" + '"' + id+ '"' + ");";
    }
}
