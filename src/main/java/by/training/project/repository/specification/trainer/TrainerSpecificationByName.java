package by.training.project.repository.specification.trainer;

public class TrainerSpecificationByName implements TrainerSpecification {
    private String surname;
    private String firstname;
    private String secname;

    public TrainerSpecificationByName(String surname, String firstname, String secname) {
        this.surname = surname;
        this.firstname = firstname;
        this.secname = secname;
    }

    @Override
    public String specify() {
        return " WHERE (surname=" + '"' + surname + '"'
                + " AND firstname = " + '"' + firstname + '"'
                + " AND secondname = " + '"' + secname + '"'  + ");";
    }
}
