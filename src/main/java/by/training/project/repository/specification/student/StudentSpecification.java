package by.training.project.repository.specification.student;

import by.training.project.entities.Course;
import by.training.project.entities.Student;
import by.training.project.repository.specification.Specification;

public interface StudentSpecification extends Specification<Student> {
    String specify();
}
