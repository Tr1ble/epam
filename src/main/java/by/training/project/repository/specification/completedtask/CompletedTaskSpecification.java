package by.training.project.repository.specification.completedtask;

import by.training.project.entities.CompletedTask;
import by.training.project.repository.specification.Specification;

public interface CompletedTaskSpecification extends Specification<CompletedTask> {
}
