package by.training.project.repository.specification.completedtask;

public class CompletedTaskSpecificationByCourse implements CompletedTaskSpecification {
    private int id;

    public CompletedTaskSpecificationByCourse(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " CROSS JOIN task ON task.id = completed_task.task_id AND task.course_id = " + id +";";
    }
}
