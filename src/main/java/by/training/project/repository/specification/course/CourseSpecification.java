package by.training.project.repository.specification.course;

import by.training.project.entities.Course;
import by.training.project.repository.specification.Specification;

public interface CourseSpecification extends Specification<Course> {
    String specify();
}
