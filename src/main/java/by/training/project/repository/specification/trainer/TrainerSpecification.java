package by.training.project.repository.specification.trainer;

import by.training.project.entities.Trainer;
import by.training.project.repository.specification.Specification;

public interface TrainerSpecification extends Specification<Trainer> {
    String specify();
}
