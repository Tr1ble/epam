package by.training.project.repository.specification.trainer;

public class TrainerSpecificationByID implements TrainerSpecification {
    private int id;

    public TrainerSpecificationByID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (id=" + '"' + id+ '"' + ");";
    }
}
