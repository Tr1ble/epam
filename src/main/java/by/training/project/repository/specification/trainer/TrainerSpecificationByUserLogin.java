package by.training.project.repository.specification.trainer;

public class TrainerSpecificationByUserLogin implements TrainerSpecification {
    private String userLogin;

    public TrainerSpecificationByUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public String specify() {
        return " WHERE (user_login=" + '"' + userLogin + '"' + ");";
    }
}
