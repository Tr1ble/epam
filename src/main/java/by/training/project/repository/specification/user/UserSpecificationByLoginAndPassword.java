package by.training.project.repository.specification.user;

import by.training.project.entities.User;

public class UserSpecificationByLoginAndPassword implements UserSpecification {
    private String login;
    private String password;

    public UserSpecificationByLoginAndPassword(String login, String password) {
        this.login = login;
        this.password = password;
    }

    @Override
    public String specify() {
        return "WHERE login=" + '"' + login+ '"' + " AND password=" + '"'+ password + '"';
    }
}

