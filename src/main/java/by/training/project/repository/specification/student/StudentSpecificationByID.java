package by.training.project.repository.specification.student;

public class StudentSpecificationByID implements StudentSpecification {
    private int id;

    public StudentSpecificationByID(int id) {
        this.id=id;
    }

    @Override
    public String specify() {
        return " WHERE (id=" + '"' + id+ '"' + ");";
    }
}
