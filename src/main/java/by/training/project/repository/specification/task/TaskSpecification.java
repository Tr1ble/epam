package by.training.project.repository.specification.task;

import by.training.project.entities.Task;
import by.training.project.repository.specification.Specification;

public interface TaskSpecification extends Specification<Task> {
}
