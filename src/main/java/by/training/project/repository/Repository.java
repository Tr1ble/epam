package by.training.project.repository;

import by.training.project.entities.Identifable;
import by.training.project.exceptions.RepositoryException;
import by.training.project.repository.specification.Specification;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * The interface Repository.
 *
 * @param <T> the type parameter
 */
public interface Repository<T extends Identifable> {
    /**
     * Add to sql.
     *
     * @param o the object to add
     * @throws RepositoryException the repository exception
     */
    void add(T o) throws RepositoryException;

    /**
     * Remove data from sql.
     *
     * @param specification the specification
     * @throws RepositoryException the repository exception
     */
    void remove(Specification<T> specification) throws RepositoryException;

    /**
     * Update data in sql.
     *
     * @param o             the object to update
     * @param specification the specification
     * @throws RepositoryException the repository exception
     */
    void update(T o,Specification<T> specification) throws RepositoryException;

    /**
     * Gets all rows from sql.
     *
     * @return the all
     * @throws RepositoryException the repository exception
     */
    List<T> getAll() throws RepositoryException;
}
