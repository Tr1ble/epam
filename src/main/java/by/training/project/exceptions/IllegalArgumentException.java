package by.training.project.exceptions;

import java.io.IOException;

public class IllegalArgumentException extends IOException {
    public IllegalArgumentException() { super("Argument is unknown!"); }
    public IllegalArgumentException(String message) { super(message);}
    public IllegalArgumentException(String message, Throwable cause) { super(message, cause); }
    public IllegalArgumentException(Throwable cause) { super(cause); }
}
