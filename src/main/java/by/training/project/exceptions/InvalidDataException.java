package by.training.project.exceptions;

import java.io.IOException;

public class InvalidDataException extends IOException {
    public InvalidDataException() { super("Data is invalid!"); }
    public InvalidDataException(String message) { super(message);}
    public InvalidDataException(String message, Throwable cause) { super(message, cause); }
    public InvalidDataException(Throwable cause) { super(cause); }
}
