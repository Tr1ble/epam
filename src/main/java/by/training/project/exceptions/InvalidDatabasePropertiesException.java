package by.training.project.exceptions;

import java.io.IOException;

public class InvalidDatabasePropertiesException extends IOException {
    public InvalidDatabasePropertiesException() { super("Database properties are invalid"); }
    public InvalidDatabasePropertiesException(String message) { super(message);}
    public InvalidDatabasePropertiesException(String message, Throwable cause) { super(message, cause); }
    public InvalidDatabasePropertiesException(Throwable cause) { super(cause); }
}
