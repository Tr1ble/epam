package by.training.project.builder;

import by.training.project.entities.Task;
import by.training.project.entities.enums.TaskEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Task builder.
 */
public class TaskBuilder implements Builder<Task> {
    @Override
    public Task build(ResultSet resultSet) throws SQLException {
        int id=resultSet.getInt(TaskEnum.ID.getValue());
        String title = resultSet.getString(TaskEnum.TITLE.getValue());
        int courseId=resultSet.getInt(TaskEnum.COURSE_ID.getValue());
        String description=resultSet.getString(TaskEnum.DESCRIPTION.getValue());
        return new Task(id, title, courseId, description);
    }
}
