package by.training.project.builder;

import by.training.project.entities.Identifable;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Functional interface for building different objects that extend Identifable interface
 * @param <T> should extend Identifable interface
 */
public interface Builder<T extends Identifable> {
    /**
     *
     * @param resultSet data from database
     * @return built object
     * @throws SQLException if errors during database request
     */
    T build(ResultSet resultSet) throws SQLException;
}
