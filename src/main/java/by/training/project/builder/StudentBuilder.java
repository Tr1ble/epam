package by.training.project.builder;

import by.training.project.entities.Student;
import by.training.project.entities.Trainer;
import by.training.project.entities.enums.StudentEnum;
import by.training.project.entities.enums.TrainerEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Student builder.
 */
public class StudentBuilder implements Builder<Student> {
    @Override
    public Student build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(StudentEnum.ID.getValue());
        String surname = resultSet.getString(StudentEnum.SURNAME.getValue());
        String firstname = resultSet.getString(StudentEnum.FIRSTNAME.getValue());
        String secondname = resultSet.getString(StudentEnum.SECONDNAME.getValue());
        int courseId = resultSet.getInt(StudentEnum.COURSE_ID.getValue());
        String userLogin = resultSet.getString(StudentEnum.USER_LOGIN.getValue());
        return new Student(firstname,surname, secondname,id, userLogin, courseId);
    }
}
