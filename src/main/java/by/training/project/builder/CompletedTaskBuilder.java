package by.training.project.builder;

import by.training.project.entities.CompletedTask;
import by.training.project.entities.Task;
import by.training.project.entities.enums.CompletedTaskEnum;
import by.training.project.entities.enums.TaskEnum;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * This builder builds completed tasks objects
 */
public class CompletedTaskBuilder implements Builder<CompletedTask> {
    @Override
    public CompletedTask build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(CompletedTaskEnum.TASK_ID.getValue());
        double mark = resultSet.getDouble(CompletedTaskEnum.MARK.getValue());
        String feedback = resultSet.getString(CompletedTaskEnum.FEEDBACK.getValue());
        String student = resultSet.getString(CompletedTaskEnum.STUDENT.getValue());
        return new CompletedTask(id, mark, feedback, student);
    }
}
