package by.training.project.builder;

import by.training.project.entities.Role;
import by.training.project.entities.User;
import by.training.project.entities.enums.UserEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type User builder.
 */
public class UserBuilder implements Builder<User> {
    @Override
    public User build(ResultSet resultSet) throws SQLException {
        int id=resultSet.getInt(UserEnum.ID.getValue());
        String login=resultSet.getString(UserEnum.LOGIN.getValue());
        String password=resultSet.getString(UserEnum.PASSWORD.getValue());
        Role role=Role.valueOf(resultSet.getString(UserEnum.ROLE.getValue()).toUpperCase());
        return new User(id, login, password, role);
    }
}
