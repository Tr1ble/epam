package by.training.project.builder;

import by.training.project.entities.Course;
import by.training.project.entities.enums.CourseEnum;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The type Course builder.
 */
public class CourseBuilder implements Builder<Course> {


    @Override
    public Course build(ResultSet resultSet) throws SQLException {
        int id=resultSet.getInt(CourseEnum.ID.getValue());
        int trainer_id=resultSet.getInt(CourseEnum.TRAINER_ID.getValue());
        String title=resultSet.getString(CourseEnum.TITLE.getValue());
        Date startDate=resultSet.getDate(CourseEnum.START_DATE.getValue());
        Date endDate=resultSet.getDate(CourseEnum.END_DATE.getValue());
        return new Course(id, trainer_id, title, startDate, endDate);
    }
}
