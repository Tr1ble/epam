package by.training.project.builder;

import by.training.project.entities.Trainer;
import by.training.project.entities.enums.TrainerEnum;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Trainers builder.
 */
public class TrainersBuilder implements Builder<Trainer> {
    @Override
    public Trainer build(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt(TrainerEnum.ID.getValue());
        String surname = resultSet.getString(TrainerEnum.SURNAME.getValue());
        String firstname = resultSet.getString(TrainerEnum.FIRSTNAME.getValue());
        String secondname = resultSet.getString(TrainerEnum.SECONDNAME.getValue());
        boolean isBusy = resultSet.getBoolean(TrainerEnum.ISBUSY.getValue());
        String userLogin = resultSet.getString(TrainerEnum.USER_LOGIN.getValue());
        return new Trainer(firstname, surname, secondname, id, isBusy, userLogin);
    }
}
