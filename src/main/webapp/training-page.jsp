<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/training.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/duDialog.css">
</head>
<body>
<header>
    <c:set var="user" value='${sessionScope["user"]}'/>
    <c:set var="student" value='${sessionScope["current-student"]}'/>
    <c:set var="trainer" value='${sessionScope["current-trainer"]}'/>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <c:if test="${user.role.value ne 'student'}">
                <a class="main-nav-item"
                   href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                    <fmt:message key="menu.trainers"/>
                </a>
            </c:if>
            <a id="current"
               class="main-nav-item" href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout" href="${pageContext.request.contextPath}/controller?command=logout">
            <img class="logout-image" src="${pageContext.request.contextPath}\stylesheets\images\logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <c:set var="taskslist" value='${sessionScope["tasks-list"]}'/>
    <c:set var="courseslist" value='${sessionScope["courses-list"]}'/>
    <c:set var="completedTaskslist" value='${sessionScope["completed-tasks-list"]}'/>
    <c:set var="number" value="0"/>
    <c:if test="${user.role.value eq 'administrator'}">
        <c:forEach items="${courseslist}" var="course">
            <div class="course-container">
                <div class="course-title"><c:out value="${course.title}"/></div>
                <div class="option, add-option">
                    <form action="${pageContext.request.contextPath}/controller?command=openAddTaskPage"
                          method="post">
                        <input type="hidden" value="${course.id}" name="courseId"/>
                        <input class="text, submit" type="submit"
                               value="<fmt:message key="options.add"/>">
                    </form>
                </div>
            </div>
            <div id="grid">
                <c:set var="flag" value='0'/>
                <c:forEach items="${taskslist}" var="task">
                    <c:if test="${task.courseId eq course.id}">
                        <c:set var="flag" value='1'/>
                        <div><c:out value="${task.title}"/></div>
                        <div><c:out value="${task.description}"/></div>
                        <form class="deleteForm" id="${task.id}" action="${pageContext.request.contextPath}/controller?command=removeTask" method="post">
                            <input type="hidden" value="${task.id}" name="taskId"/>
                            <img class="delete" id="${task.id}"
                                   src="${pageContext.request.contextPath}\stylesheets\images\cross.png"/>
                        </form>

                    </c:if>
                </c:forEach>
            </div>
            <c:if test="${flag eq '0'}">
                <div class="message-container">
                    <div class="empty_list_hint">
                        <em>
                            <fmt:message key="messages.empty_tasks_list"/>
                        </em>
                    </div>
                </div>
            </c:if>
        </c:forEach>
    </c:if>
    <c:if test="${user.role.value ne 'administrator'}">
        <c:set var="course_flag" value='0'/>
        <c:forEach items="${courseslist}" var="course">
            <c:if test="${trainer ne null}">
                <c:if test="${user.role.value eq'trainer'}">
                    <c:if test="${course.trainerId eq trainer.id}">
                        <c:set var="course_flag" value='1'/>
                        <div class="course-container">
                            <div class="course-title"><c:out value="${course.title}"/></div>
                            <div class="option, history-option">
                                <form action="${pageContext.request.contextPath}/controller?command=openHistoryPage"
                                      method="post">
                                    <input type="hidden" value="${course.id}" name="courseId"/>
                                    <input onclick="openHistoryWindow()" class="text, submit" type="submit"
                                           value="<fmt:message key="training.history"/>">
                                </form>
                            </div>
                        </div>
                        <div class="trainer-grid">
                            <c:set var="flag" value='0'/>
                            <c:forEach items="${taskslist}" var="task">
                                <c:if test="${task.courseId eq course.id}">
                                    <c:set var="flag" value='1'/>
                                    <div><c:out value="${task.title}"/></div>
                                    <div><c:out value="${task.description}"/></div>
                                    <div class="option">
                                        <form action="${pageContext.request.contextPath}/controller?command=openVerifyPage"
                                              method="post">
                                            <input type="hidden" value="${task.id}" name="taskId"/>
                                            <input type="hidden" value="${course.id}" name="courseId"/>
                                            <input class="text, submit" type="submit"
                                                   value="<fmt:message key="training.verify"/>">
                                        </form>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                        <c:if test="${flag eq '0'}">
                            <div class="message-container">
                                <div class="empty_list_hint">
                                    <em>
                                        <fmt:message key="messages.empty_tasks_list"/>
                                    </em>
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                </c:if>
            </c:if>
            <c:if test="${student ne null}">
                <c:if test="${user.role.value eq'student'}">
                    <c:if test="${course.id eq student.courseId}">
                        <c:set var="course_flag" value='1'/>
                        <div class="course-container">
                            <div class="course-title"><c:out value="${course.title}"/></div>
                        </div>
                        <div class="student-grid">
                            <c:set var="flag" value='0'/>
                            <c:forEach items="${taskslist}" var="task">
                                <c:if test="${task.courseId eq course.id}">
                                    <c:set var="flag" value='1'/>
                                    <div><c:out value="${task.title}"/></div>
                                    <div><c:out value="${task.description}"/></div>
                                    <c:set var="completedtaskflag" value='0'/>
                                    <c:forEach items="${completedTaskslist}" var="completedTask">
                                        <c:if test="${task.id eq completedTask.id}">
                                            <c:if test="${completedTask.student eq student.user}">
                                                <c:set var="completedtaskflag" value='1'/>
                                                <div id="green" class="tooltip">
                                                        ${completedTask.mark}
                                                    <span class="tooltiptext">
                                                            ${completedTask.feedback}
                                                    </span>
                                                </div>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                    <c:if test="${completedtaskflag eq '0'}">
                                        <div id="red">
                                            <fmt:message key="message.not_completed"/>
                                        </div>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                        </div>
                        <c:if test="${flag eq '0'}">
                            <div class="message-container">
                                <div class="empty_list_hint">
                                    <em>
                                        <fmt:message key="messages.empty_tasks_list"/>
                                    </em>
                                </div>
                            </div>
                        </c:if>
                    </c:if>
                </c:if>
            </c:if>
        </c:forEach>
        <c:if test="${(user.role.value eq 'trainer') && (course_flag eq '0')}">
            <fmt:message key="messages.not_a_trainer"/>
        </c:if>
        <c:if test="${(user.role.value eq 'student') && (course_flag eq '0')}">
            <fmt:message key="messages.not_a_student"/>
        </c:if>
    </c:if>
</div>
</body>
<script src="scripts/duDialog.js">
</script>
</html>
