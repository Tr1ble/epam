<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/courses.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/duDialog.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
    </head>
<body>
<header>
    <c:set var="user" value='${sessionScope["user"]}'/>
    <c:set var="student" value='${sessionScope["current-student"]}'/>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item" id="current"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <c:if test="${user.role.value ne 'student'}">
                <a class="main-nav-item"
                   href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                    <fmt:message key="menu.trainers"/>
                </a>
            </c:if>
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout" href="${pageContext.request.contextPath}/controller?command=logout">
            <img class="logout-image" src="${pageContext.request.contextPath}\stylesheets\images\logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <c:if test="${user.role.value eq 'administrator'}">
        <div class="options">
            <a href="${pageContext.request.contextPath}/controller?command=openAddCoursePage">
                <div class="add-button">
                </div>
            </a>
        </div>
    </c:if>
    <div id="grid">
        <div>
            <fmt:message key="courses.trainer_id"/>
        </div>
        <div>
            <fmt:message key="courses.title"/>
        </div>
        <div>
            <fmt:message key="courses.start_date"/>
        </div>
        <div>
            <fmt:message key="courses.end_date"/>
        </div>
        <div>
        </div>
        <div>
        </div>
        <jsp:useBean id="date" class="java.util.Date"/>
        <c:set var="list" value='${sessionScope["courses-list"]}'/>
        <c:set var="trainersList" value='${sessionScope["busy-trainer-list"]}'/>
        <c:forEach items="${list}" var="item">
            <c:forEach items="${trainersList}" var="trainer">
                <c:if test="${item.trainerId eq trainer.id}">
                    <div><c:out value="${trainer.surname}"/> <c:out value="${trainer.firstname}"/>
                        <c:out value="${trainer.secondname}"/></div>
                </c:if>
            </c:forEach>
            <div><c:out value="${item.title}"/></div>
            <div><fmt:formatDate value="${item.startDate}" pattern="dd.MM.yyyy"/></div>
            <div><fmt:formatDate value="${item.endDate}" pattern="dd.MM.yyyy"/></div>
            <c:if test="${user.role.value eq 'administrator'}">
                <form class="deleteForm" id="${item.id}" action="${pageContext.request.contextPath}/controller?command=removeCourse" method="post">
                    <input type="hidden" value="${item.id}" name="courseId"/>
                    <input type="hidden" value="${item.trainerId}" name="trainerId"/>
                    <img class="delete" id=${item.id}"
                         src="${pageContext.request.contextPath}\stylesheets\images\cross.png"/>
                </form>
                <form class="editForm" action="${pageContext.request.contextPath}/controller?command=openEditCoursePage" method="post">
                    <input type="hidden" value="${item.id}" name="courseId"/>
                    <input class="edit" type="image"
                         src="${pageContext.request.contextPath}\stylesheets\images\edit.svg"/>
                </form>
            </c:if>
            <c:if test="${user.role.value eq 'student'}">
                <c:if test="${student eq null}">
                    <div class="form">
                        <form action="${pageContext.request.contextPath}/controller?command=openRegistrationPage"
                              method="post">
                            <input type="hidden" value="${item.id}" name="courseId"/>
                            <input class="text, submit" type="submit"
                                   value="<fmt:message key="training.registration"/>"
                                   align="center">
                        </form>
                    </div>
                    <div></div>
                </c:if>
                <c:if test="${student ne null}">
                    <c:if test="${student.courseId eq item.id}">
                        <div class="form">
                            <form action="${pageContext.request.contextPath}/controller?command=unregister"
                                  method="post">
                                <input type="hidden" value="${item.id}" name="courseId"/>
                                <input class="text, submit" type="submit"
                                       value="<fmt:message key="training.leave"/>"
                                       align="center">
                            </form>
                        </div>
                        <div></div>
                    </c:if>
                    <c:if test="${student.courseId ne item.id}">
                        <div>
                        </div>
                        <div></div>
                    </c:if>
                </c:if>
            </c:if>
            <c:if test="${user.role.value eq 'trainer'}">
                <div>
                </div>
                <div>
                </div>
            </c:if>
        </c:forEach>
    </div>
</div>
</body>
<script src="scripts/duDialog.js"></script>
</html>

