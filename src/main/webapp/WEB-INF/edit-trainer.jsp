<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/trainers.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
</head>
<body>
<header>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <c:if test="${user.role.value ne 'student'}">
                <a class="main-nav-item" id="current"
                   href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                    <fmt:message key="menu.trainers"/>
                </a>
            </c:if>
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout">
            <img class="logout-image" src="https://icon-library.net/images/logout-icon-png/logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <div class="form">
        <c:set var="trainerId" value='${requestScope["trainerId"]}'/>
        <c:set value='${requestScope["surname"]}' var="surname"/>
        <c:set value='${requestScope["firstname"]}' var="firstname"/>
        <c:set value='${requestScope["secondname"]}' var="secondname"/>
        <c:set value='${requestScope["userLogin"]}' var="userLogin"/>
        <c:set value='${requestScope["isBusy"]}' var="isBusy"/>
        <form action="${pageContext.request.contextPath}/controller?command=editTrainer" method="post">
            <input type="hidden" value="${trainerId}" name="trainerId"/>
            <input type="hidden" value="${isBusy}" name="isBusy"/>
            <input class="text" type="text" value="${surname}" placeholder="<fmt:message key="trainers.surname"/>" name="surname"/>
            <input class="text" type="text" value="${firstname}" placeholder="<fmt:message key="trainers.firstname"/>" name="firstname"/>
            <input class="text"  type="text" value="${secondname}" placeholder="<fmt:message key="trainers.secondname"/>" name="secondname"/>
            <input class="text" type="text" value="${userLogin}" placeholder="<fmt:message key="trainers.user_id"/>" name="userLogin"/>
            <input class="text, submit" type="submit"  value="<fmt:message key="options.save"/>" align="center">
        </form>
    </div>
</div>
</body>
</html>
