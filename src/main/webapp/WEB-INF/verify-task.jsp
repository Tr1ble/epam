<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/trainers.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
</head>
<body>
<header>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                <fmt:message key="menu.trainers"/>
            </a>
            <a class="main-nav-item"
               id="current"
               href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout">
            <img class="logout-image" src="https://icon-library.net/images/logout-icon-png/logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <div class="form">
        <c:set var="courseId" value='${sessionScope["courseId"]}'/>
        <c:set var="taskId" value='${sessionScope["taskId"]}'/>
        <c:set var="studentsList" value='${sessionScope["students-list"]}'/>
        <c:set var="completedTasksList" value='${sessionScope["completed-tasks-list"]}'/>
        <form action="${pageContext.request.contextPath}/controller?command=verifyTask" method="post">
            <p><fmt:message key="completed_task.student"/>
                <select class="select" name="student">
                    <c:forEach items="${studentsList}" var="student">
                        <c:if test="${student.courseId eq courseId}">
                            <c:set var="flag" value='0'/>
                            <c:forEach items="${completedTasksList}" var="completedTask">
                                <c:if test="${student.user eq completedTask.student}">
                                    <c:if test="${completedTask.id eq taskId}">
                                        <c:set var="flag" value='1'/>
                                    </c:if>
                                </c:if>
                            </c:forEach>
                            <c:if test="${flag eq '0'}">
                                <option>
                                    <c:out value="${student.surname}"/> <c:out value="${student.firstname}"/> <c:out value="${student.secondname}"/>
                                </option>
                            </c:if>
                        </c:if>
                    </c:forEach>
                </select>
            </p>
            <input class="text" type="number" min="1" max="10" placeholder="<fmt:message key="completed_task.mark"/>"
                   name="mark"/>
            <input class="text" type="text" placeholder="<fmt:message key="completed_task.feedback"/>" name="feedback"/>
            <input type="hidden" value="${taskId}" name="taskId"/>
            <input class="text, submit" type="submit" value="<fmt:message key="training.verify"/>">
        </form>
    </div>
</div>
</body>
</html>
