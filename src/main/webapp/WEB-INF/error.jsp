
<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>

<html>
<head>
    <title><fmt:message key="messages.error"/> </title>
</head>
<body>
    <c:set var="errorMessage" value='${requestScope["errorMessage"]}'/>
    <c:out value="Error: ${errorMessage}"/>
</body>
</html>
