<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="lang" class="java.lang.String" scope="session"/>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/courses.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
</head>
<body>
<header>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item" id="current"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <c:if test="${user.role.value ne 'student'}">
                <a class="main-nav-item"
                   href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                    <fmt:message key="menu.trainers"/>
                </a>
            </c:if>
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout">
            <img class="logout-image" src="https://icon-library.net/images/logout-icon-png/logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <div class="form">
        <form action="${pageContext.request.contextPath}/controller?command=editCourse" method="post">
            <p> <fmt:message key="messages.trainer_id"/>
            <select class="select" name="trainer">
                <c:set value='${requestScope["surname"]}' var="surname"/>
                <c:set value='${requestScope["name"]}' var="firstname"/>
                <c:set value='${requestScope["secname"]}' var="secondname"/>
                <option>
                    <c:out value="${surname}"/> <c:out value="${firstname}"/> <c:out value="${secondname}"/>
                </option>
                <c:set var="list" value='${sessionScope["trainers-list"]}'/>
                <c:forEach items="${list}" var="item">
                    <c:if test="${item.busy eq false}">
                    <option>
                        <c:out value="${item.surname}"/> <c:out value="${item.firstname}"/> <c:out value="${item.secondname}"/>
                    </option>
                    </c:if>
                </c:forEach>
            </select>
            </p>
            <c:set var="title" value='${requestScope["title"]}'/>
            <c:set var="courseId" value='${requestScope["courseId"]}'/>
            <c:set var="startDate" value='${requestScope["startDate"]}'/>
            <c:set var="endDate" value='${requestScope["endDate"]}'/>
            <input value="${courseId}" type="hidden" name="courseId"/>
            <input value="${surname} ${firstname} ${secondname}" type="hidden" name="oldTrainer"/>
            <input class="text" value="${title}" type="text" placeholder="<fmt:message key="courses.title"/>" name="title"/>
            <input class="text" value="${startDate}" type="date" lang="${lang}" placeholder="<fmt:message key="courses.start_date"/>" name="startDate"/>
            <input class="text" value="${endDate}" type="date" placeholder="<fmt:message key="courses.end_date"/>" name="endDate"/>
            <input class="text, submit" type="submit"  value="<fmt:message key="options.save"/>" align="center">
        </form>
    </div>
</div>
</body>
</html>
