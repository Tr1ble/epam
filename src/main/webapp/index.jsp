<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<%@ page isELIgnored="false" %>
<%@ page session="true" %>


<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="stylesheets/common.css">
    <link rel="stylesheet" href="stylesheets/auth.css">
</head>
<body>
<header>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <lng:lang-selection/>
</header>
<c:set var="errorMessage" value='${requestScope["errorMessage"]}'/>
<div class="main-container">
    <div class="form">
        <form action="${pageContext.request.contextPath}/controller?command=login" method="post">
            <input class="text" type="text" placeholder="<fmt:message key="auth.username"/>" name="username"/>
            <input class="text" type="password" placeholder="<fmt:message key="auth.password"/>" name="password"/>
            <c:if test="${errorMessage ne null}">
                <div class="error">${errorMessage}</div>
            </c:if>
            <input class="text, submit" type="submit" value="<fmt:message key="auth.login"/>" align="center">
        </form>
    </div>
</div>
</body>
</html>