<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/history.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
</head>
<body>
<header>
    <c:set var="user" value='${sessionScope["user"]}'/>
    <c:set var="student" value='${sessionScope["current-student"]}'/>
    <c:set var="trainer" value='${sessionScope["current-trainer"]}'/>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg"
         width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <c:if test="${user.role.value ne 'student'}">
                <a class="main-nav-item"
                   href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                    <fmt:message key="menu.trainers"/>
                </a>
            </c:if>
            <a id="current"
               class="main-nav-item" href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout" href="${pageContext.request.contextPath}/controller?command=logout">
            <img class="logout-image" src="https://icon-library.net/images/logout-icon-png/logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <div class="table-container">
        <c:if test="${user.role.value eq 'trainer'}">
            <display:table name="sessionScope.completed-tasks-list" pagesize="10"
                           sort="list" uid="one" class="history-table">
                <display:column property="student" title="Student"
                                sortable="true" headerClass="sortable"/>
                <display:column property="mark" title="Mark"
                                sortable="true" headerClass="sortable"/>
                <display:column property="feedback" title="Feedback"
                                sortable="false" headerClass="sortable"/>
            </display:table>

        </c:if>
    </div>
</div>
</body>
</html>
