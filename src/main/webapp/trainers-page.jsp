<%@ page contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="lng" uri="languagetag" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="content"/>


<html lang="${sessionScope.lang}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>
        <fmt:message key="main.label"/>
    </title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/trainers.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/duDialog.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/stylesheets/main.css">
</head>
<body>
<header>
    <img alt="training_logo" class="logo" src="${pageContext.request.contextPath}\stylesheets\images\training.svg" width="200"
         height="100">
    <nav class="main-nav">
        <ul class="main-nav-list">
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openCoursesPage">
                <fmt:message key="menu.courses"/>
            </a>
            <a class="main-nav-item" id="current"
               href="${pageContext.request.contextPath}/controller?command=openTrainersPage">
                <fmt:message key="menu.trainers"/>
            </a>
            <a class="main-nav-item"
               href="${pageContext.request.contextPath}/controller?command=openTrainingPage">
                <fmt:message key="menu.training"/>
            </a>
        </ul>
    </nav>
    <lng:lang-selection/>
    <div class="logout-container">
        <a class="logout" href="${pageContext.request.contextPath}/controller?command=logout">
            <img class="logout-image" src="${pageContext.request.contextPath}\stylesheets\images\logout-icon-png-6.jpg"
                 height="100px">
        </a>
    </div>
</header>
<div class="main-container">
    <c:if test="${user.role.value eq 'administrator'}">
        <div class="options">
            <a href="${pageContext.request.contextPath}/controller?command=openAddTrainerPage">
                <div class="add-button">
                </div>
            </a>
        </div>
    </c:if>
    <div id="grid">
        <div>
            <fmt:message key="trainers.surname"/>
        </div>
        <div>
            <fmt:message key="trainers.firstname"/>
        </div>
        <div>
            <fmt:message key="trainers.secondname"/>
        </div>
        <div>
            <fmt:message key="trainers.busy"/>
        </div>
        <div>
            <fmt:message key="trainers.user_id"/>
        </div>
        <div>
        </div>
        <div>
        </div>
        <c:set var="number" value="0"/>
        <c:set var="list" value='${sessionScope["trainers-list"]}'/>
        <c:forEach items="${list}" var="item">
            <div><c:out value="${item.surname}"/></div>
            <div><c:out value="${item.firstname}"/></div>
            <div><c:out value="${item.secondname}"/></div>
            <c:choose>
                <c:when test="${item.busy eq false}">
                    <div><fmt:message key="messages.no"/> </div>
                </c:when>
                <c:otherwise>
                    <c:set var="coursesList" value='${sessionScope["courses-list"]}'/>
                    <c:forEach items="${coursesList}" var="course">
                        <c:if test="${course.trainerId eq item.id}">
                            <div><c:out value="${course.title}"/></div>
                        </c:if>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
            <div><c:out value="${item.userLogin}"/></div>
            <form class="deleteForm" id="${item.id}" action="${pageContext.request.contextPath}/controller?command=removeTrainer" method="post">
                <input type="hidden" value="${item.id}" name="trainerId"/>
                <c:if test="${user.role.value eq 'administrator'}">
                    <img class="delete" id="${item.id}"
                         src="${pageContext.request.contextPath}\stylesheets\images\cross.png"/>
                </c:if>
            </form>
            <form class="editForm" action="${pageContext.request.contextPath}/controller?command=openEditTrainerPage" method="post">
                <c:if test="${user.role.value eq 'administrator'}">
                <input type="hidden" value="${item.id}" name="trainerId"/>
                <input class="edit" type="image"
                       src="${pageContext.request.contextPath}\stylesheets\images\edit.svg"/>
                </c:if>
            </form>
        </c:forEach>
    </div>
</div>
</body>
<script src="scripts/duDialog.js"></script>
</html>
